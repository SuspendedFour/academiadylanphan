#ifndef CUBE_H
#define CUBE_H

class Cube : public Geode
{
public:
    void draw(Matrix4 matrix)
    {
        if (color)
        {
            glColor3f(color[0], color[1], color[2]);
        }

        glLoadMatrixd((matrix * transform).getPointer());
        glutSolidCube(1.0);
    }
};

#endif