#ifndef SPHERE_H
#define SPHERE_H

#include "Geode.h"

class Sphere : public Geode
{
public:
    void draw(Matrix4 matrix)
    {
        if (color)
        {
            glColor3f(color[0], color[1], color[2]);
        }

        glLoadMatrixd((matrix * transform).getPointer());
        glutSolidSphere(1.0, 6, 6);
    }
};

#endif