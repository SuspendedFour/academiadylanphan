#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>

#include <GL/glut.h>
#include "Matrix4.h"
#include "Sphere.h"
#include "Cube.h"
#include "MatrixTransform.h"
#include "TrackballController.h"
#include "Robot.h"

#define PI 3.141592653589793238462

static int width  = 512;   // set window width in pixels here
static int height = 512;   // set window height in pixels here

static TrackballController trackballController;

static MatrixTransform* world;
static Group* robotGroup;
static Robot** robots;

static double left = -10.0;
static double right = 10.0;
static double bottom = -10.0;
static double top = 10.0;
static double near = 10;
static double far = 1000.0;

static int robotCount = 200;

static int lastMessage = 0;
static int lastFrame = 0;

static bool cullingEnabled = false;
static bool animateEnabled = false;

void reshapeCallback(int w, int h)
{
    width = w;
    height = h;
    glViewport(0, 0, w, h);  // set new viewport size

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(left, right, bottom, top, near, far);
    glTranslatef(0, 0, -20);
}

void displayCallback(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers
    glMatrixMode(GL_MODELVIEW);

    double start = clock();

	for (int i = 0; i < robotCount; ++i)
    {
        robots[i]->scaleFactor = trackballController.scale;
        if (animateEnabled)
        {
            robots[i]->update();
        }
    }

    world->transform = trackballController.rotation * Matrix4::scaling(trackballController.scale, trackballController.scale, trackballController.scale);
    world->draw(Matrix4::identity());

    double end = clock();

    if (end - lastMessage > CLOCKS_PER_SEC)
    {
        lastMessage = end;
        std::cout << (end - start) / CLOCKS_PER_SEC * 1000 << std::endl;
    }

    glFlush();
    glutSwapBuffers();
}

void mouseCallback(int button, int state, int x, int y)
{
    trackballController.mouseCallback(button, state, x, y);
}

void motionCallback(int x, int y)
{
    trackballController.motionCallback(x, y, width, height);
}

void keyboardCallback(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'c':
            cullingEnabled = !cullingEnabled;
            for (int i = 0; i < robotCount; ++i)
            {
                robots[i]->cullingEnabled = cullingEnabled;
            }

            break;
        case 'a':
            animateEnabled = !animateEnabled;
            break;
    }
}

int main(int argc, char *argv[])
{
    float specular[]  = {1.0, 1.0, 1.0, 1.0};
    float shininess[] = {100.0};

    glutInit(&argc, argv);                      // initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
    glutInitWindowSize(width, height);      // set initial window size
    glutCreateWindow("OpenGL Cube for CSE167");           // open window and set window title

    glEnable(GL_DEPTH_TEST);                    // enable depth buffering
    glClear(GL_DEPTH_BUFFER_BIT);               // clear depth buffer
    glClearColor(0.0, 0.0, 0.0, 0.0);           // set clear color to black
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
    glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
    glShadeModel(GL_SMOOTH);                    // set shading to smooth
    glMatrixMode(GL_PROJECTION);
    glEnable(GL_NORMALIZE);

    // Generate material properties:
    // glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    // glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    // Generate light source:
    // glEnable(GL_LIGHTING);

    // Install callback functions:
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(reshapeCallback);
    glutIdleFunc(displayCallback);
    glutKeyboardFunc(keyboardCallback);
    // glutSpecialFunc(specialCallback);
    glutMouseFunc(mouseCallback);
    glutMotionFunc(motionCallback);

    world = new MatrixTransform();
    robotGroup = new Group();
    world->children.push_back(robotGroup);

    robots = new Robot*[robotCount];
    for (int i = 0; i < robotCount; ++i)
    {
        int x = i % 5 - 2;
        int y = i / 5 - 2;
        robots[i] = new Robot(left, right, bottom, top, near, far);
        robots[i]->transform = Matrix4::translation(x, 0, -y);
        robotGroup->children.push_back(robots[i]);
    }

    glutMainLoop();

    return 0;
}

