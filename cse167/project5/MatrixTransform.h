#ifndef MATRIX_TRANSFORM_H
#define MATRIX_TRANSFORM_H

#include "Group.h"

class MatrixTransform : public Group
{
public:
    Matrix4 transform;
    MatrixTransform() : transform(Matrix4::identity()) { }

    void draw(Matrix4 matrix)
    {
        Group::draw(matrix * transform);
    }
};

#endif