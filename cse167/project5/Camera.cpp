#include "Camera.h"

Camera::Camera() { }

Camera::Camera(Vector3 e, Vector3 d, Vector3 up)
{
    Vector3 z = (e - d).nor();
    Vector3 x = up.cross(z).nor();
    Vector3 y = z.cross(x);
    Matrix4 rotation = Matrix4(x.toVector4Vector(), y.toVector4Vector(), z.toVector4Vector(), Vector4(0, 0, 0, 1)).transpose();
    Matrix4 translation = Matrix4::translation(e.neg());
    matrix = rotation * translation;
}

Matrix4 Camera::getMatrix()
{
    return matrix;
}

double* Camera::getGLMatrix()
{
    return matrix.getPointer();
}