#ifndef GEODE_H
#define GEODE_H

#include "Node.h"

class Geode : public Node
{
public:
    float* color;
    Matrix4 transform;
    Geode() : transform(Matrix4::identity())
    {
        color = new float[3];
        color[0] = rand() % 100 * .01;
        color[1] = rand() % 100 * .01;
        color[2] = rand() % 100 * .01;
    }
};

#endif