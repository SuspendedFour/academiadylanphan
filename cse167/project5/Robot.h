#ifndef ROBOT_H
#define ROBOT_H

#include "Cube.h"
#include "Geode.h"
#include "Matrix4.h"
#include "MatrixTransform.h"
#include "Plane.h"

#define PI 3.141592653589793238462

class Robot : public Geode
{
private:
    double angle;
    double angularVelocity;

    double left;
    double right;
    double bottom;
    double top;
    double near;
    double far;

    double torsoHeight;
    double torsoWidth;
    double torsoDepth;
    double headHeight;
    double headWidth;
    double headDepth;
    double limbWidth;
    double totalArmHeight;
    double totalLegHeight;
    double armHeight;
    double legHeight;

    MatrixTransform* root;

    MatrixTransform* headGroup;
    MatrixTransform* leftArmGroup;
    MatrixTransform* rightArmGroup;
    MatrixTransform* leftHandGroup;
    MatrixTransform* rightHandGroup;
    MatrixTransform* leftLegGroup;
    MatrixTransform* rightLegGroup;
    MatrixTransform* leftFootGroup;
    MatrixTransform* rightFootGroup;

    std::vector<Plane> clippingPlanes;

public:
    double scaleFactor;
    bool cullingEnabled;
    Robot(double left, double right, double bottom, double top, double near, double far)
        : angle(0), angularVelocity(.1), scaleFactor(1), left(left), right(right), bottom(bottom), top(top), near(near), far(far), cullingEnabled(false)
    {
        Vector3 leftPlaneNormal = Vector3(left, top, near).cross(Vector3(left, bottom, near)).nor();
        Vector3 rightPlaneNormal = Vector3(right, bottom, near).cross(Vector3(right, top, near)).nor();
        Vector3 topPlaneNormal = Vector3(right, top, near).cross(Vector3(left, top, near)).nor();
        Vector3 bottomPlaneNormal = Vector3(left, bottom, near).cross(Vector3(right, bottom, near)).nor();
        Vector3 nearPlaneNormal = Vector3(0, 0, 1);
        Vector3 farPlaneNormal = Vector3(0, 0, -1);

        clippingPlanes.push_back(Plane(Vector3(), leftPlaneNormal, "left"));
        clippingPlanes.push_back(Plane(Vector3(), rightPlaneNormal, "right"));
        clippingPlanes.push_back(Plane(Vector3(), topPlaneNormal, "top"));
        clippingPlanes.push_back(Plane(Vector3(), bottomPlaneNormal, "bottom"));
        clippingPlanes.push_back(Plane(Vector3(0, 0, near), nearPlaneNormal, "near"));
        clippingPlanes.push_back(Plane(Vector3(0, 0, far), farPlaneNormal, "far"));

        torsoHeight = 1.0;
        torsoWidth = torsoHeight * .75;
        torsoDepth = torsoWidth / 3;
        headWidth = torsoWidth / 2;
        headHeight = headWidth * 1.25;
        headDepth = headHeight * .666;

        limbWidth = torsoWidth * .25;
        totalArmHeight = torsoHeight * .9;
        totalLegHeight = torsoHeight * .5;
        armHeight = totalArmHeight - limbWidth;
        legHeight = totalLegHeight - limbWidth;

        root = new MatrixTransform();

        headGroup = new MatrixTransform();
        leftArmGroup = new MatrixTransform();
        rightArmGroup = new MatrixTransform();
        leftHandGroup = new MatrixTransform();
        rightHandGroup = new MatrixTransform();
        leftLegGroup = new MatrixTransform();
        rightLegGroup = new MatrixTransform();
        leftFootGroup = new MatrixTransform();
        rightFootGroup = new MatrixTransform();

        Cube* torso = new Cube();
        Sphere* head = new Sphere();
        Cube* leftArm = new Cube();
        Cube* rightArm = new Cube();
        Sphere* leftHand = new Sphere();
        Sphere* rightHand = new Sphere();
        Cube* leftLeg = new Cube();
        Cube* rightLeg = new Cube();
        Sphere* leftFoot = new Sphere();
        Sphere* rightFoot = new Sphere();

        updateGroups();

        torso->transform = Matrix4::scaling(torsoWidth, torsoHeight, torsoDepth);
        head->transform = Matrix4::scaling(headWidth, headHeight, headDepth);
        leftArm->transform = Matrix4::scaling(limbWidth, armHeight, torsoDepth);
        rightArm->transform = Matrix4::scaling(limbWidth, armHeight, torsoDepth);
        leftHand->transform = Matrix4::scaling(limbWidth, limbWidth, limbWidth);
        rightHand->transform = Matrix4::scaling(limbWidth, limbWidth, limbWidth);
        leftLeg->transform = Matrix4::scaling(limbWidth, legHeight, torsoDepth);
        rightLeg->transform = Matrix4::scaling(limbWidth, legHeight, torsoDepth);
        leftFoot->transform = Matrix4::scaling(limbWidth, limbWidth, limbWidth);
        rightFoot->transform = Matrix4::scaling(limbWidth, limbWidth, limbWidth);

        headGroup->children.push_back(head);
        leftArmGroup->children.push_back(leftArm);
        rightArmGroup->children.push_back(rightArm);
        leftHandGroup->children.push_back(leftHand);
        rightHandGroup->children.push_back(rightHand);
        leftLegGroup->children.push_back(leftLeg);
        rightLegGroup->children.push_back(rightLeg);
        leftFootGroup->children.push_back(leftFoot);
        rightFootGroup->children.push_back(rightFoot);

        leftArmGroup->children.push_back(leftHandGroup);
        rightArmGroup->children.push_back(rightHandGroup);
        leftLegGroup->children.push_back(leftFootGroup);
        rightLegGroup->children.push_back(rightFootGroup);

        root->children.push_back(torso);
        root->children.push_back(headGroup);
        root->children.push_back(leftArmGroup);
        root->children.push_back(rightArmGroup);
        root->children.push_back(leftLegGroup);
        root->children.push_back(rightLegGroup);
    }

    void draw(Matrix4 matrix)
    {
        if (cullingEnabled)
        {
            Vector3 sphereCenter = (matrix * transform * Vector4(0, 0, 0, 1)).toVector3() + Vector3(0, 0, 40);
            double sphereRadius = 1.1 * scaleFactor;
            for (auto i = clippingPlanes.begin(); i != clippingPlanes.end(); ++i)
            {
                double dist = (sphereCenter - i->point).dot(i->normal);
                if (dist < -sphereRadius) return;
            }
        }

        root->draw(matrix * transform);
    }

    void update()
    {
        angle = (angle + angularVelocity);
        if (abs(angle) > PI / 5) angularVelocity *= -1;

        updateGroups();
    }

    void updateGroups()
    {
        headGroup->transform = Matrix4::translation(0, torsoHeight / 2 + headHeight / 4, 0) * Matrix4::yAxisRotation(angle);

        leftArmGroup->transform = Matrix4::translation(-torsoWidth / 2, torsoHeight / 2 - armHeight / 2, 0)
            * Matrix4::translationSandwich(Matrix4::xAxisRotation(angle), Vector3(0, armHeight / 2, 0));

        rightArmGroup->transform = Matrix4::translation(torsoWidth / 2, torsoHeight / 2 - armHeight / 2, 0)
            * Matrix4::translationSandwich(Matrix4::xAxisRotation(-angle), Vector3(0, armHeight / 2, 0));

        leftLegGroup->transform = Matrix4::translation(-torsoWidth / 4, -torsoHeight / 2 - legHeight / 4, 0)
            * Matrix4::translationSandwich(Matrix4::xAxisRotation(-angle), Vector3(0, legHeight / 2, 0));

        rightLegGroup->transform = Matrix4::translation(torsoWidth / 4, -torsoHeight / 2 - legHeight / 4, 0)
            * Matrix4::translationSandwich(Matrix4::xAxisRotation(angle), Vector3(0, legHeight / 2, 0));

        leftHandGroup->transform = Matrix4::translation(0, -armHeight / 2, 0) * Matrix4::zAxisRotation(angle);
        rightHandGroup->transform = Matrix4::translation(0, -armHeight / 2, 0) * Matrix4::zAxisRotation(angle);
        leftFootGroup->transform = Matrix4::translation(0, -legHeight / 2, 0) * Matrix4::yAxisRotation(angle);
        rightFootGroup->transform = Matrix4::translation(0, -legHeight / 2, 0) * Matrix4::yAxisRotation(angle);
    }
};

#endif