#ifndef TRACKBALLCONTROLLER_H
#define TRACKBALLCONTROLLER_H

class TrackballController
{
private:
    int mouseX;
    int mouseY;
    int mouseButton;

public:
    Matrix4 rotation;
    double scale;

    TrackballController()
    {
        rotation = Matrix4::identity();
        scale = 1;
    }

    void mouseCallback(int button, int state, int x, int y)
    {
        if (state != GLUT_DOWN) return;
        mouseButton = button;
        mouseX = x;
        mouseY = y;
    }

    void motionCallback(int x, int y, int width, int height)
    {
        if (x == mouseX && y == mouseY) return;
        if (mouseButton== GLUT_LEFT_BUTTON)
        {
            rotation = rotation * rotation.trackballRotation(width, height, mouseX, mouseY, x, y);
        }
        else if (mouseButton == GLUT_RIGHT_BUTTON)
        {
            double factor = 1 + .05 * (y < mouseY ? 1 : -1);
            scale *= factor;
            // matrix = Matrix4::scaling(factor, factor, factor) * matrix;
        }

        mouseX = x;
        mouseY = y;
    }
};

#endif