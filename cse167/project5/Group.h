#ifndef GROUP_H
#define GROUP_H

#include "Node.h"
#include <vector>

class Group : public Node
{
public:
    std::vector<Node*> children;

    void draw(Matrix4 matrix)
    {
        for (auto node = children.begin(); node != children.end(); ++node)
        {
            (*node)->draw(matrix);
        }
    }
};

#endif