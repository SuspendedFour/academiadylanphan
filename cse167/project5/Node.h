#ifndef NODE_H
#define NODE_H

#include "Matrix4.h"

class Node
{
public:
    virtual void draw(Matrix4 matrix) { };
};

#endif