#include "Vector4.h"
#include <iostream>
#include <stdexcept>

using namespace std;

Vector4::Vector4(double x, double y, double z)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = 1;
}

Vector4::Vector4(double x, double y, double z, double w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

void Vector4::setX(double x)
{
    this->x = x;
}

void Vector4::setY(double y)
{
    this->y = y;
}

void Vector4::setZ(double z)
{
    this->z = z;
}

void Vector4::setW(double w)
{
    this->w = w;
}

double Vector4::getX()
{
    return this->x;
}

double Vector4::getY()
{
    return this->y;
}

double Vector4::getZ()
{
    return this->z;
}

double Vector4::getW()
{
    return this->w;
}

double Vector4::operator[](int dimension) const
{
    switch (dimension)
    {
        case 0:
            return this->x;
            break;
        case 1:
            return this->y;
            break;
        case 2:
            return this->z;
            break;
        case 3:
            return this->w;
            break;
        default:
            throw runtime_error("Cannot subscript Vector4 with index larger than 3.");
    }
}

Vector4 Vector4::add(const Vector4& other) const
{
    return Vector4(this->x + other.x, this->y + other.y, this->z + other.z, this->w + other.w);
}

Vector4 Vector4::operator+(const Vector4& other) const
{
    return Vector4(this->x + other.x, this->y + other.y, this->z + other.z, this->w + other.w);
}

Vector4 Vector4::sub(const Vector4& other) const
{
    return Vector4(this->x - other.x, this->y - other.y, this->z - other.z, this->w - other.w);
}

Vector4 Vector4::operator-(const Vector4& other) const
{
    return Vector4(this->x - other.x, this->y - other.y, this->z - other.z, this->w - other.w);
}

Vector4 Vector4::dehomogenize() const
{
    return Vector4(this->x / this->w, this->y / this->w, this->z / this->w, 1);
}

void Vector4::print() const
{
    cout << "(" << x << "," << y << "," << z << "," << w << ")" << endl;
}

Vector3 Vector4::toVector3() const
{
    return Vector3(x, y, z);
}