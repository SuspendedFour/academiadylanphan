#ifndef PLANE_H
#define PLANE_H

#include "Vector3.h"

class Plane
{
public:
    Vector3 point;
    Vector3 normal;
    char* tag;
    Plane(Vector3 point, Vector3 normal) : point(point), normal(normal) { }
    Plane(Vector3 point, Vector3 normal, char* tag) : point(point), normal(normal), tag(tag) { }
};

#endif