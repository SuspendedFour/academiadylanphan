#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>

#include <GL/glut.h>
#include "Vector3.h"
#include "Matrix4.h"

#include "TrackballController.h"

#define PI 3.141592653589793238462

static int width  = 512;   // set window width in pixels here
static int height = 512;   // set window height in pixels here

static const double pointRadius = 15;
static const int controlPointCount = 7;
static Vector3 controlPoints[controlPointCount];
static int activeControlIndex = -1;

static bool textureEnabled = false;
static bool editModeActive = true;
static TrackballController trackballController;

static const double tDelta = 1.0 / 20.0;
static const double angleDelta = 10.0 / 360.0 * 2 * PI;

/** Load a ppm file from disk.
 @input filename The location of the PPM file.  If the file is not found, an error message
        will be printed and this function will return 0
 @input width This will be modified to contain the width of the loaded image, or 0 if file not found
 @input height This will be modified to contain the height of the loaded image, or 0 if file not found
 @return Returns the RGB pixel data as interleaved unsigned chars (R0 G0 B0 R1 G1 B1 R2 G2 B2 .... etc) or 0 if an error ocured
**/
unsigned char* loadPPM(const char* filename, int& width, int& height)
{
    const int BUFSIZE = 128;
    FILE* fp;
    unsigned int read;
    unsigned char* rawData;
    char buf[3][BUFSIZE];
    char* retval_fgets;
    size_t retval_sscanf;

    if ( (fp=fopen(filename, "rb")) == NULL)
    {
        std::cerr << "error reading ppm file, could not locate " << filename << std::endl;
        width = 0;
        height = 0;
        return NULL;
    }

    // Read magic number:
    retval_fgets = fgets(buf[0], BUFSIZE, fp);

    // Read width and height:
    do
    {
        retval_fgets=fgets(buf[0], BUFSIZE, fp);
    } while (buf[0][0] == '#');
    retval_sscanf=sscanf(buf[0], "%s %s", buf[1], buf[2]);
    width  = atoi(buf[1]);
    height = atoi(buf[2]);

    // Read maxval:
    do
    {
      retval_fgets=fgets(buf[0], BUFSIZE, fp);
    } while (buf[0][0] == '#');

    // Read image data:
    rawData = new unsigned char[width * height * 3];
    read = fread(rawData, width * height * 3, 1, fp);
    fclose(fp);
    if (read != 1)
    {
        std::cerr << "error parsing ppm file, incomplete data" << std::endl;
        delete[] rawData;
        width = 0;
        height = 0;
        return NULL;
    }

    return rawData;
}


// load image file into texture object
void loadTexture()
{
    GLuint texture[1];     // storage for one texture
    int twidth, theight;   // texture width/height [pixels]
    unsigned char* tdata;  // texture pixel data

    // Load image file
    tdata = loadPPM("image.ppm", twidth, theight);
    if (tdata==NULL) return;

    // Create ID for texture
    glGenTextures(1, &texture[0]);   

    // Set this texture to be the one we are working with
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    // Generate the texture
    glTexImage2D(GL_TEXTURE_2D, 0, 3, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, tdata);

    // Set bi-linear filtering for both minification and magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Vector3 worldToScreen(Vector3 world)
{
    double x = (world.x + 1) / 2 * width;
    double y = (1 - world.y) / 2 * height;
    return Vector3(x, y, 0);
}

Vector3 screenToWorld(double screenX, double screenY)
{
    double x = screenX / width * 2 - 1;
    double y = 1 - (screenY / height * 2);
    return Vector3(x, y, 1);
}

void drawControlPoints()
{
    glPointSize(pointRadius);
    glBegin(GL_POINTS);
    for (int i = 0; i < controlPointCount; ++i)
    {
		if (i % 3 == 0)
		{
			glColor3f(0, 0, 1);
		}
		else
		{
			glColor3f(1, 1, 1);
		}

        Vector3 controlPoint = controlPoints[i].scale(10);
        glVertex3f(controlPoint.x, controlPoint.y, controlPoint.z);
    }
    glEnd();

    glColor3f(1, 1, 1);
    glBegin(GL_LINES);
    for (int i = 0; i < controlPointCount - 1; ++i)
    {
        Vector3 start = controlPoints[i].scale(10);
        Vector3 end = controlPoints[i + 1].scale(10);
        glVertex3f(start.x, start.y, start.z);
        glVertex3f(end.x, end.y, end.z);
    }

    glEnd();
}

double bezier(double A,  // Start value
              double B,  // First control value
              double C,  // Second control value
              double D,  // Ending value
              double t)  // Parameter 0 <= t <= 1
{
    double s = 1 - t;
    double AB = A*s + B*t;
    double BC = B*s + C*t;
    double CD = C*s + D*t;
    double ABC = AB*s + CD*t;
    double BCD = BC*s + CD*t;
    return ABC*s + BCD*t;
}

double bezierPrime(double p0, double p1, double p2, double p3, double t)
{
    double dt = sqrt(DBL_EPSILON);
    // return bezier(p0, p1, p2, p3, t + dt) - bezier(p0, p1, p2, p3, t - dt) / (2 * dt);
    return
        -3 * (1 - 2 * t + t * t) * p0 -
        3 * t * (4 - 3 * t) * p1 +
        3 * t * (2 - 3 * t) * p2 +
        3 * t * t * p3;
}

void drawCurves()
{
    glColor3f(.5, .5, .5);
    glBegin(GL_LINES);
    for (int i = 0; i < controlPointCount - 1; i += 3)
    {
        for (double t = 0; t < 1; t += tDelta)
        {
            double xStart = bezier(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t);
            double yStart = bezier(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t);
            double xEnd = bezier(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t + tDelta);
            double yEnd = bezier(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t + tDelta);
            glVertex3f(xStart * 10, yStart * 10, 10);
            glVertex3f(xEnd * 10, yEnd * 10, 10);
        }
    }

    glEnd();
}

void drawSurface()
{
    if (textureEnabled)
    {
        glEnable(GL_TEXTURE_2D);
    }
    else
    {
        glEnable(GL_LIGHTING);
    }

    glEnable(GL_LIGHT0);
    glColor3f(1, 1, 1);
	glBegin(GL_QUADS);
    for (int i = 0; i < controlPointCount - 1; i += 3)
    {
        for (double t = 0; t < 1; t += tDelta)
        {
            double xStartRaw = bezier(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t);
            double yStartRaw = bezier(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t);
            double xEndRaw = bezier(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t + tDelta);
            double yEndRaw = bezier(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t + tDelta);

            double xStart = xStartRaw * 20;
            double yStart = yStartRaw * 20;
            double xEnd = xEndRaw * 20;
            double yEnd = yEndRaw * 20;
            double xStartPrime = bezierPrime(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t);
            double yStartPrime = bezierPrime(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t);
            double xEndPrime = bezierPrime(controlPoints[i].x, controlPoints[i + 1].x, controlPoints[i + 2].x, controlPoints[i + 3].x, t + tDelta);
            double yEndPrime = bezierPrime(controlPoints[i].y, controlPoints[i + 1].y, controlPoints[i + 2].y, controlPoints[i + 3].y, t + tDelta);
            Vector4 startNormal = Vector3(-yStartPrime, xStartPrime, 0).nor().toVector4Vector();
            Vector4 endNormal = Vector3(-yEndPrime, xEndPrime, 0).nor().toVector4Vector();

            double yStartTex = t / 2;
            double yEndTex = (t + tDelta) / 2;
            if (i > 0)
            {
                yStartTex += .5;
                yEndTex += .5;
            }

            for (double angle = 0; angle < 2 * PI; angle += angleDelta)
            {
                Matrix4 translation = Matrix4::translation(0, 0, -20);
                Matrix4 rotation1 = Matrix4::yAxisRotation(angle);
                Vector4 start1 = translation * rotation1 * Vector4(xStart, yStart, 20, 1);
                Vector4 end1 = translation * rotation1 * Vector4(xEnd, yEnd, 20, 1);
                Vector3 rotatedStartNormal1 = (rotation1 * startNormal).toVector3().nor();
                Vector3 rotatedEndNormal1 = (rotation1 * endNormal).toVector3().nor();

                Matrix4 rotation2 = Matrix4::yAxisRotation(angle + angleDelta);
                Vector4 start2 = translation * rotation2 * Vector4(xStart, yStart, 20, 1);
                Vector4 end2 = translation * rotation2 * Vector4(xEnd, yEnd, 20, 1);
                Vector3 rotatedStartNormal2 = (rotation2 * startNormal).toVector3().nor();
                Vector3 rotatedEndNormal2 = (rotation2 * endNormal).toVector3().nor();

                glNormal3f(rotatedStartNormal2.x, rotatedStartNormal2.y, rotatedStartNormal2.z);
                glTexCoord2f((angle + angleDelta) / (2 * PI), yStartTex);
                glVertex3f(start2.x, start2.y, start2.z);

                glNormal3f(rotatedEndNormal2.x, rotatedEndNormal2.y, rotatedEndNormal2.z);
                glTexCoord2f((angle + angleDelta) / (2 * PI), yEndTex);
                glVertex3f(end2.x, end2.y, end2.z);

                glNormal3f(rotatedEndNormal1.x, rotatedEndNormal1.y, rotatedEndNormal1.z);
                glTexCoord2f(angle / (2 * PI), yEndTex);
                glVertex3f(end1.x, end1.y, end1.z);

                glNormal3f(rotatedStartNormal1.x, rotatedStartNormal1.y, rotatedStartNormal1.z);
                glTexCoord2f(angle / (2 * PI), yStartTex);
                glVertex3f(start1.x, start1.y, start1.z);
            }
        }
    }

    glEnd();
}

void reshapeCallback(int w, int h)
{
    width = w;
    height = h;
    glViewport(0, 0, w, h);  // set new viewport size

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-10.0, 10.0, -10.0, 10.0, 10, 1000.0); // set perspective projection viewing frustum
    glTranslatef(0, 0, -20);
}

void displayCallback(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);

    Matrix4 modelView = Matrix4::translationSandwich(trackballController.rotation, Vector3(0, 0, -20));
    glLoadMatrixd(modelView.getPointer());

    // Draw axis of rotation
    if (editModeActive)
    {
    	glColor3f(1, 1, 1);
        glBegin(GL_LINES);
        glVertex3f(0, 10, 9.99);
        glVertex3f(0, -10, 9.99);
        glEnd();
        
        drawControlPoints();
        drawCurves();
    }

    drawSurface();

    glFlush();
    glutSwapBuffers();
}

void mouseCallback(int button, int state, int x, int y)
{
    if (editModeActive)
    {
        if (state == GLUT_DOWN)
        {
            for (int i = 0; i < controlPointCount; ++i)
            {
                Vector3 controlPoint = controlPoints[i];
                Vector3 screenPoint = worldToScreen(controlPoint);
                if ((Vector3(x, y, 0) - screenPoint).mag() < pointRadius)
                {
                    activeControlIndex = i;
                }
            }
        }
        else
        {
            activeControlIndex = -1;
        }
    }
    else
    {
        trackballController.mouseCallback(button, state, x, y);
    }
}

void motionCallback(int x, int y)
{
    if (editModeActive)
    {
        if (activeControlIndex != -1)
        {
            controlPoints[activeControlIndex] = screenToWorld(std::max(width / 2, x), y);
        }
    }
    else
    {
        trackballController.motionCallback(x, y, width, height);
    }
}

void keyboardCallback(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'r':
            editModeActive = !editModeActive;
            if (editModeActive)
            {
                trackballController.rotation = Matrix4::identity();
            }

            break;
        case 't':
            textureEnabled = !textureEnabled;
            break;
    }
}

int main(int argc, char *argv[])
{
    // float specular[]  = {1.0, 1.0, 1.0, 1.0};
    // float shininess[] = {100.0};

    glutInit(&argc, argv);                      // initialize GLUT
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
    glutInitWindowSize(width, height);      // set initial window size
    glutCreateWindow("OpenGL Cube for CSE167");           // open window and set window title

    glEnable(GL_DEPTH_TEST);                    // enable depth buffering
    glClear(GL_DEPTH_BUFFER_BIT);               // clear depth buffer
    glClearColor(0.0, 0.0, 0.0, 0.0);           // set clear color to black
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
    glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
    glShadeModel(GL_SMOOTH);                    // set shading to smooth
    glMatrixMode(GL_PROJECTION);
    glEnable(GL_NORMALIZE);

    // Generate material properties:
    // glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    // glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    // glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    // glEnable(GL_COLOR_MATERIAL);

    // Generate light source:
    glEnable(GL_LIGHTING);
    // float position[] = {-.2, .5, .5, 0};
    float position[] = {0, 0, 1, 0};
    float diffuse[] = {.2, .7, .8, 1};
    float specular[] = {.8, .3, .2, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);

    // Install callback functions:
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(reshapeCallback);
    glutIdleFunc(displayCallback);
    glutKeyboardFunc(keyboardCallback);
    // glutSpecialFunc(specialCallback);
    glutMouseFunc(mouseCallback);
    glutMotionFunc(motionCallback);

    controlPoints[0] = Vector3(.0, .8, 1);
    controlPoints[1] = Vector3(.2, .6, 1);
    controlPoints[2] = Vector3(.4, .4, 1);
    controlPoints[3] = Vector3(.5, .0, 1);
    controlPoints[4] = Vector3(.3, -.2, 1);
    controlPoints[5] = Vector3(.6, -.5, 1);
    controlPoints[6] = Vector3(.3, -.8, 1);

    loadTexture();

    glutMainLoop();

    return 0;
}

