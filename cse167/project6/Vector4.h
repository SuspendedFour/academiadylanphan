#ifndef VECTOR_4_H
#define VECTOR_4_H

#include "Vector3.h"

class Vector3;
class Vector4
{
    private:
    public:
        double x;
        double y;
        double z;
        double w;
        Vector4(double, double, double);
        Vector4(double, double, double, double);
        void setX(double);
        void setY(double);
        void setZ(double);
        void setW(double);
        double getX();
        double getY();
        double getZ();
        double getW();
        double operator[](int) const;
        Vector4 add(const Vector4&) const;
        Vector4 operator+(const Vector4&) const;
        Vector4 sub(const Vector4&) const;
        Vector4 operator-(const Vector4&) const;
        Vector4 dehomogenize() const;
        void print() const;
        Vector3 toVector3() const;
};

#endif