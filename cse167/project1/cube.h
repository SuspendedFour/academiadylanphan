#ifndef _CUBE_H_
#define _CUBE_H_

#include "Matrix4.h"

class Cube
{
  private:
    Matrix4 matrix;                 // model matrix
    double angularVelocity;
    double angle;                   // rotation angle [degrees]
    double zAngle;
    Vector3 position;
    double speed;
    double scaleFactor;

  public:
    float r;
    float g;
    float b;

    Cube();   // Constructor
    Matrix4& getMatrix();
    void update();
    void changeDirection();
    void moveLeft();
    void moveRight();
    void moveDown();
    void moveUp();
    void moveIn();
    void moveOut();
    void rotateZ(double radians);
    void scale(double);
    void color(float, float, float);
    void reset();
    void printPosition();
};

class Window	  // output window related routines
{
  public:
    static int width, height; 	            // window size

    static void idleCallback(void);
    static void reshapeCallback(int, int);
    static void displayCallback(void);
    static void keyboardCallback(unsigned char, int, int);
};

#endif

