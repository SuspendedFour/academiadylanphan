#ifndef DRAW_DATA_H
#define DRAW_DATA_H

class DrawData
{
public:
    float* vertices;
    int nVertices;
    int nIndices;
    int* indices;
};

#endif