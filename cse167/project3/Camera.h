#ifndef CAMERA_H
#define CAMERA_H

#include "Vector3.h"
#include "Matrix4.h"

class Camera
{
private:
    Matrix4 matrix;
public:
    Camera();
    Camera(Vector3 e, Vector3 d, Vector3 up);
    double* getGLMatrix();
    Matrix4 getMatrix();
};

#endif