#ifndef _CUBE_H_
#define _CUBE_H_

#include "Matrix4.h"
#include "Camera.h"

class Cube
{
  public:
    float r;
    float g;
    float b;

    Cube();   // Constructor
    void changeDirection();
    void moveLeft();
    void moveRight();
    void moveDown();
    void moveUp();
    void moveIn();
    void moveOut();
    void rotateZ(double radians);
    void scale(double);
    void color(float, float, float);
    void reset();
    void printPosition();
};

class Window	  // output window related routines
{
  public:
    static int width, height; 	            // window size

    static void idleCallback(void);
    static void reshapeCallback(int, int);
    static void drawCube();
    static void drawHouse();
	static void drawTexture();
    static void displayCallback(void);
    static void keyboardCallback(unsigned char, int, int);
    static void specialCallback(int, int, int);
	static void loadTexture(char*);
};

#endif

