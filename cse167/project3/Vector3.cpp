#include "Vector3.h"
#include <stdexcept>
#include <cmath>
#include <iostream>

using namespace std;

Vector3::Vector3(): x(0), y(0), z(0) { };

Vector3::Vector3(double x, double y, double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void Vector3::setX(double x)
{
    this->x = x;
}

void Vector3::setY(double y)
{
    this->y = y;
}

void Vector3::setZ(double z)
{
    this->z = z;
}

double Vector3::getX()
{
    return this->x;
}

double Vector3::getY()
{
    return this->y;
}

double Vector3::getZ()
{
    return this->z;
}

double Vector3::operator[](int dimension) const
{
    switch (dimension)
    {
        case 0:
            return this->x;
            break;
        case 1:
            return this->y;
            break;
        case 2:
            return this->z;
            break;
        default:
            throw runtime_error("Cannot subscript Vector3 with index larger than 2.");
    }
}

Vector3 Vector3::add(const Vector3& other) const
{
    return Vector3(this->x + other.x, this->y + other.y, this->z + other.z);
}

Vector3 Vector3::operator+(const Vector3& other) const
{
    return Vector3(this->x + other.x, this->y + other.y, this->z + other.z);
}

Vector3 Vector3::operator+=(const Vector3& rhs)
{
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
}

Vector3 Vector3::sub(const Vector3& other) const
{
    return Vector3(this->x - other.x, this->y - other.y, this->z - other.z);
}

Vector3 Vector3::operator-(const Vector3& other) const
{
    return Vector3(this->x - other.x, this->y - other.y, this->z - other.z);
}

Vector3 Vector3::operator-=(const Vector3& rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
}

Vector3 Vector3::neg() const
{
    return Vector3(-this->x, -this->y, -this->z);
}

Vector3 Vector3::scale(double factor) const
{
    return Vector3(factor * this->x, factor * this->y, factor * this->z);
}

double Vector3::dot(const Vector3& other) const
{
    return this->x * other.x + this->y * other.y + this->z * other.z;
}

Vector3 Vector3::cross(const Vector3& other) const
{
    return Vector3(this->y * other.z - this->z * other.y, this->z * other.x - this->x * other.z, this->x * other.y - this->y * other.x);
}

double Vector3::mag() const
{
    return sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
}

Vector3 Vector3::nor() const
{
    double magnitude = this->mag();
    return Vector3(this->x / magnitude, this->y / magnitude, this->z / magnitude);
}

void Vector3::print() const
{
    cout << "(" << x << "," << y << "," << z << ")" << endl;
}

Vector4 Vector3::toVector4Point() const
{
    return Vector4(x, y, z, 1);
}

Vector4 Vector3::toVector4Vector() const
{
    return Vector4(x, y, z, 0);
}