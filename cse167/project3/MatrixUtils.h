#ifndef _MATRIXUTILS_H_
#define _MATRIXUTILS_H_

#include "Matrix4.h"

class MatrixUtils
{
public:
    static Matrix4 createPerspective(double aspectRatio, double fieldOfViewRadians, double near, double far);
    static Matrix4 createViewport(double width, double height);
    static Matrix4 createCamera(Vector3 e, Vector3 d, Vector3 up);
};

#endif
