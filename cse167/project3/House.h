#ifndef HOUSE_H
#define HOUSE_H

class House
{
public:
  static int nVerts;
  static float vertices[];
  static float colors[];
  static int indicesLength;
  static int indices[];
};
#endif