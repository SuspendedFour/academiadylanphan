#ifndef VECTOR_3_H
#define VECTOR_3_H

#include "Vector4.h"

class Vector4;
class Vector3
{
    public:
        double x;
        double y;
        double z;
        Vector3();
        Vector3(double, double, double);
        void setX(double);
        void setY(double);
        void setZ(double);
        double getX();
        double getY();
        double getZ();
        double operator[](int) const;
        Vector3 add(const Vector3&) const;
        Vector3 operator+(const Vector3&) const;
        Vector3 operator+=(const Vector3&);
        Vector3 sub(const Vector3&) const;
        Vector3 operator-(const Vector3&) const;
        Vector3 operator-=(const Vector3&);
        Vector3 neg() const;
        Vector3 scale(double) const;
        double dot(const Vector3& other) const;
        Vector3 cross(const Vector3&) const;
        double mag() const;
        Vector3 nor() const;
        void print() const;
		Vector4 toVector4Point() const;
        Vector4 toVector4Vector() const;
};

#endif