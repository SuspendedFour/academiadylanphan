#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <GL/glut.h>
#include "cube.h"
#include "Matrix4.h"
#include "House.h"
#include "objreader.h"
#include "MatrixUtils.h"

#define PI 3.141592653589793238462

using namespace std;

static int window_width = 512, window_height = 512;

static double angularVelocity = .001;
static double angle = 0.0;
static double zAngle = 0;
static Vector3 position;
static double speed = .5;
static double scaleFactor = 1;

static Matrix4 objectToWorld;
static bool fullscreenActive = false;

static float fullScale;
static float minX;
static float maxX;
static float minY;
static float maxY;
static float minZ;
static float maxZ;

static Cube cube;
static Camera camera;
static void (*render)() = Window::drawCube;
static float* vertices;
static int nVertices;
static int nIndices;
static int* indices;
static float* texcoords;
static float* normals;

int Window::width  = 512;   // set window width in pixels here
int Window::height = 512;   // set window height in pixels here

//----------------------------------------------------------------------------
// Callback method called when system is idle.
void Window::idleCallback(void)
{
  angle += angularVelocity;
  if (angle > 2 * PI || angle < -2 * PI) angle = 0.0;
  objectToWorld = Matrix4::zAxisRotation(zAngle) *
      Matrix4::translation(position) *
      Matrix4::yAxisRotation(angle) *
      Matrix4::scaling(scaleFactor, scaleFactor, scaleFactor);

  displayCallback();  // call display routine to re-draw cube
}

//----------------------------------------------------------------------------
// Callback method called when window is resized.
void Window::reshapeCallback(int w, int h)
{
  width = w;
  height = h;
  glViewport(0, 0, w, h);  // set new viewport size
  // glMatrixMode(GL_PROJECTION);
  // glLoadIdentity();
  // glFrustum(-10.0, 10.0, -10.0, 10.0, 10, 1000.0); // set perspective projection viewing frustum
  // glTranslatef(0, 0, -20);
  // glMatrixMode(GL_MODELVIEW);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-10.0, 10.0, -10.0, 10.0, 10, 1000.0);
  glTranslatef(0, 0, -20);
}

//----------------------------------------------------------------------------
// Callback method called when window readraw is necessary or
// when glutPostRedisplay() was called.
void Window::displayCallback(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers
  glMatrixMode(GL_MODELVIEW);

  render();

  glFlush();  
  glutSwapBuffers();
}

void Window::drawCube()
{
  glLoadMatrixd(objectToWorld.getPointer());
  glBegin(GL_QUADS);
    glColor3f(cube.r, cube.g, cube.b);

    // Draw front face:
    glNormal3f(0.0, 0.0, 1.0);   
    glVertex3f(-5.0,  5.0,  5.0);
    glVertex3f( 5.0,  5.0,  5.0);
    glVertex3f( 5.0, -5.0,  5.0);
    glVertex3f(-5.0, -5.0,  5.0);

    // Draw left side:
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-5.0,  5.0,  5.0);
    glVertex3f(-5.0,  5.0, -5.0);
    glVertex3f(-5.0, -5.0, -5.0);
    glVertex3f(-5.0, -5.0,  5.0);
    
    // Draw right side:
    glNormal3f(1.0, 0.0, 0.0);
    glVertex3f( 5.0,  5.0,  5.0);
    glVertex3f( 5.0,  5.0, -5.0);
    glVertex3f( 5.0, -5.0, -5.0);
    glVertex3f( 5.0, -5.0,  5.0);
  
    // Draw back face:
    glNormal3f(0.0, 0.0, -1.0);
    glVertex3f(-5.0,  5.0, -5.0);
    glVertex3f( 5.0,  5.0, -5.0);
    glVertex3f( 5.0, -5.0, -5.0);
    glVertex3f(-5.0, -5.0, -5.0);
  
    // Draw top side:
    glNormal3f(0.0, 1.0, 0.0);
    glVertex3f(-5.0,  5.0,  5.0);
    glVertex3f( 5.0,  5.0,  5.0);
    glVertex3f( 5.0,  5.0, -5.0);
    glVertex3f(-5.0,  5.0, -5.0);
  
    // Draw bottom side:
    glNormal3f(0.0, -1.0, 0.0);
    glVertex3f(-5.0, -5.0, -5.0);
    glVertex3f( 5.0, -5.0, -5.0);
    glVertex3f( 5.0, -5.0,  5.0);
    glVertex3f(-5.0, -5.0,  5.0);
  glEnd();
}

void Window::drawHouse()
{
  // Matrix4 camera1 = MatrixUtils::createCamera(Vector3(0, 10, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
  // Matrix4 projection = MatrixUtils::createPerspective(window_width / window_height, PI / 4, 0, 1000);
  // // Matrix4 viewport = MatrixUtils::createViewport(window_width, window_height);
  // // Matrix4 matrix = projection * camera1;
  // Matrix4 matrix = projection * Matrix4::translation(0, 0, -20) * camera1;

  glLoadMatrixd(camera.getGLMatrix());
  glBegin(GL_TRIANGLES);
  for (int i = 0; i < House::indicesLength; i++)
  {
    int idx = House::indices[i];
    glColor3f(House::colors[idx * 3], House::colors[idx * 3 + 1], House::colors[idx * 3 + 2]);
    glVertex3f(House::vertices[idx * 3], House::vertices[idx * 3 + 1], House::vertices[idx * 3 + 2]);
  }

  glEnd();
}

void Window::drawTexture()
{
  if (fullscreenActive)
  {
    float minX = vertices[0];
    float maxX = vertices[0];
    float minY = vertices[1];
    float maxY = vertices[1];
    float minZ = vertices[2];
    float maxZ = vertices[2];

    for (int i = 0; i < nVertices; i++)
    {
      minX = min(minX, vertices[i * 3]);
      maxX = max(maxX, vertices[i * 3]);
      minY = min(minY, vertices[i * 3 + 1]);
      maxY = max(maxY, vertices[i * 3 + 1]);
      minZ = min(minZ, vertices[i * 3 + 2]);
      maxZ = max(maxZ, vertices[i * 3 + 2]);
    }

    float centerX = minX + (maxX - minX) / 2;
    float centerY = minY + (maxY - minY) / 2;
    float centerZ = minZ + (maxZ - minZ) / 2;

    objectToWorld =
        Matrix4::scaling(fullScale, fullScale, fullScale) *
        Matrix4::zAxisRotation(zAngle) *
        Matrix4::yAxisRotation(angle) *
        Matrix4::translation(-centerX, -centerY, -centerZ);
  }

  Matrix4 glMatrix = camera.getMatrix() * objectToWorld;
  glLoadMatrixd(glMatrix.getPointer());
  glBegin(GL_TRIANGLES);
  for (int i = 0; i < nIndices; i++)
  {
    int idx = indices[i];
    if (i % 2 == 0)
    {
      glColor3f(1, 0, 0);
    }
    else
    {
      glColor3f(0, 0, 1);
    }

    glVertex3f(vertices[idx * 3], vertices[idx * 3 + 1], vertices[idx * 3 + 2]);
  }

  glEnd();
}

void Window::keyboardCallback(unsigned char key, int x, int y)
{
  switch (key)
  {
    case 'c':
      cube.changeDirection();
      break;
    case 'x':
      cube.moveLeft();
      break;
    case 'X':
      cube.moveRight();
      break;
    case 'y':
      cube.moveDown();
      break;
    case 'Y':
      cube.moveUp();
      break;
    case 'z':
      cube.moveIn();
      break;
    case 'Z':
      cube.moveOut();
      break;
    case 'a':
      cube.rotateZ(.1);
      break;
    case 'A':
      cube.rotateZ(-.1);
      break;
    case 's':
      cube.scale(.9);
      break;
    case 'S':
      cube.scale(1.1);
      break;
    case '1':
      cube.color(1, 0, 0);
      break;
    case '2':
      cube.color(0, 1, 0);
      break;
    case '3':
      cube.color(0, 0, 1);
      break;
    case '4':
      cube.color(1, 1, 0);
      break;
    case 'r':
      cube.reset();
      break;
    case 'f':
      fullscreenActive = !fullscreenActive;
      if (fullscreenActive && render == Window::drawTexture)
      {
        float minX = vertices[0];
        float maxX = vertices[0];
        float minY = vertices[1];
        float maxY = vertices[1];
        float minZ = vertices[2];
        float maxZ = vertices[2];

        for (int i = 0; i < nVertices; i++)
        {
          minX = min(minX, vertices[i * 3]);
          maxX = max(maxX, vertices[i * 3]);
          minY = min(minY, vertices[i * 3 + 1]);
          maxY = max(maxY, vertices[i * 3 + 1]);
          minZ = min(minZ, vertices[i * 3 + 2]);
          maxZ = max(maxZ, vertices[i * 3 + 2]);
        }

        cout << "min x: " << minX << ", ";
        cout << "max X: " << maxX << ", ";
        cout << "min Y: " << minY << ", ";
        cout << "max Y: " << maxY << ", ";
        cout << "min Z: " << minZ << ", ";
        cout << "max Z: " << maxZ << endl;

        float centerX = minX + (maxX - minX) / 2;
        float centerY = minY + (maxY - minY) / 2;
        float centerZ = minZ + (maxZ - minZ) / 2;

        cout << "Translation matrix used for centering:" << endl;
        Matrix4::translation(-centerX, -centerY, -centerZ).print();

        cout << "Scale matrix:" << endl;
        Matrix4::scaling(fullScale, fullScale, fullScale).print();
      }

      break;
  }

  cube.printPosition();
}

void Window::specialCallback(int key, int x, int y)
{
  switch (key)
  {
    case GLUT_KEY_F1:
      Window::loadTexture("sphere.obj");
      fullScale = 46.403;
      break;
    case GLUT_KEY_F2:
      Window::loadTexture("teddy.obj");
      fullScale = 1.15;
      break;
    case GLUT_KEY_F3:
      Window::loadTexture("teapot.obj");
      fullScale = 7;
      break;
    case GLUT_KEY_F4:
      Window::loadTexture("cow.obj");
      fullScale = 4;
      break;
    case GLUT_KEY_F5:
      Window::loadTexture("bunny.obj");
      fullScale = 15;
      break;
    case GLUT_KEY_F6:
      render = Window::drawCube;
      break;
    case GLUT_KEY_F8:
      render = Window::drawHouse;
      camera = Camera(Vector3(0, 10, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
      break;
    case GLUT_KEY_F9:
      render = Window::drawHouse;
      camera = Camera(Vector3(-15, 5, 10), Vector3(-5, 0, 0), Vector3(0, 1, 0.5));
      break;
  }
}

void Window::loadTexture(char* filename)
{
  camera = Camera(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
  ObjReader::readObj(filename, nVertices, &vertices, &normals, &texcoords, nIndices, &indices);
  render = Window::drawTexture;
}

Cube::Cube(): r(1) { }

void Cube::changeDirection()
{
  angularVelocity = -angularVelocity;
}

void Cube::moveLeft()
{
  position += Vector3(-speed, 0, 0);
}

void Cube::moveRight()
{
  position += Vector3(speed, 0, 0);
}

void Cube::moveDown()
{
  position += Vector3(0, -speed, 0);
}

void Cube::moveUp()
{
  position += Vector3(0, speed, 0);
}

void Cube::moveIn()
{
  position += Vector3(0, 0, -speed);
}

void Cube::moveOut()
{
  position += Vector3(0, 0, speed);
}

void Cube::rotateZ(double radians)
{
  zAngle += radians;
  if (zAngle > 2 * PI || zAngle < -2 * PI) zAngle = 0.0;
}

void Cube::scale(double factor)
{
  scaleFactor *= factor;
  cout << "Scale: " << scaleFactor << endl;
}

void Cube::color(float r, float g, float b)
{
  this->r = r;
  this->g = g;
  this->b = b;
}

void Cube::reset()
{
  position = Vector3();
  scaleFactor = 1;
  zAngle = 0;
}

void Cube::printPosition()
{
  position.print();
}

int main2(int argc, char *argv[])
{
  float specular[]  = {1.0, 1.0, 1.0, 1.0};
  float shininess[] = {100.0};
  float position[]  = {0.0, 10.0, 1.0, 0.0};	// lightsource position
  
  glutInit(&argc, argv);      	      	      // initialize GLUT
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
  glutInitWindowSize(Window::width, Window::height);      // set initial window size
  glutCreateWindow("OpenGL Cube for CSE167");    	      // open window and set window title

  glEnable(GL_DEPTH_TEST);            	      // enable depth buffering
  glClear(GL_DEPTH_BUFFER_BIT);       	      // clear depth buffer
  glClearColor(0.0, 0.0, 0.0, 0.0);   	      // set clear color to black
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
  glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
  glShadeModel(GL_SMOOTH);             	      // set shading to smooth
  glMatrixMode(GL_PROJECTION); 
  glEnable(GL_NORMALIZE);
  
  // Generate material properties:
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);
  
  // Generate light source:
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glDisable(GL_LIGHTING);
  
  // Install callback functions:
  glutDisplayFunc(Window::displayCallback);
  glutReshapeFunc(Window::reshapeCallback);
  glutIdleFunc(Window::idleCallback);
  glutKeyboardFunc(Window::keyboardCallback);
  glutSpecialFunc(Window::specialCallback);

  glutMainLoop();

  return 0;
}

