#ifdef _WIN32
  #include <windows.h>
#endif

#include <iostream>
#include <math.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <float.h>
#include <time.h>

#include "House.h"
#include "Matrix4.h"
#include "MatrixUtils.h"
#include "DrawData.h"
#include "objreader.h"

#define PI 3.141592653589793238462

static int window_width = 512, window_height = 512;
static float* pixels = new float[window_width * window_height * 3];
static double* zBuffer;

static double angularVelocity = .1;
static double angle = 0.0;
static double zAngle = 0;
static Vector3 position;
static double speed = .5;
static double scaleFactor = 1;

static Matrix4 objectToWorld;

static float minX;
static float maxX;
static float minY;
static float maxY;
static float minZ;
static float maxZ;

static DrawData drawDatas[5];
static int drawMode = 7;
static Matrix4 camera;
static Matrix4 model;

static float* vertices;
static int nVertices;
static int nIndices;
static int* indices;
static float* texcoords;
static float* normals;

static bool optimize = false;
static bool boundaries = false;

static float cubeVertices[] = {
  // Draw front face:
   -5.0,  5.0,  5.0,
    5.0,  5.0,  5.0,  
    5.0, -5.0,  5.0,
   -5.0, -5.0,  5.0,

  // Draw left side:
   -5.0,  5.0,  5.0,
   -5.0,  5.0, -5.0,
   -5.0, -5.0, -5.0,
   -5.0, -5.0,  5.0,
    
  // Draw right side:
    5.0,  5.0,  5.0,
    5.0,  5.0, -5.0,
    5.0, -5.0, -5.0,
    5.0, -5.0,  5.0,
  
  // Draw back face:
   -5.0,  5.0, -5.0,
    5.0,  5.0, -5.0,
    5.0, -5.0, -5.0,
   -5.0, -5.0, -5.0,
  
  // Draw top side:
   -5.0,  5.0,  5.0,
    5.0,  5.0,  5.0,
    5.0,  5.0, -5.0,
   -5.0,  5.0, -5.0,
  
  // Draw bottom side:
   -5.0, -5.0, -5.0,
    5.0, -5.0, -5.0,
    5.0, -5.0,  5.0,
   -5.0, -5.0,  5.0};

static int cubeIndices[] = {
          0,1,2,    0,2,3,
          4,5,6,    4,7,8,
          8,9,10,  8,10,11,
          12,13,14, 12,15,14,
          16,17,18, 16,18,19,
          20,21,22, 20,23,22};

using namespace std;

struct Color    // generic color
{
  float r,g,b;
};

DrawData loadTexture(char* filename)
{
  DrawData result;
  float* texcoords;
  float* normals;
  ObjReader::readObj(filename, result.nVertices, &result.vertices, &normals, &texcoords, result.nIndices, &result.indices);
  return result;
}

void loadData()
{
  DrawData cubeData;
  cubeData.vertices = cubeVertices;
  cubeData.nVertices = 24;
  cubeData.indices = cubeIndices;
  cubeData.nIndices = 36;
  drawDatas[0] = cubeData;
  drawDatas[1] = loadTexture("sphere.obj");
  drawDatas[2] = loadTexture("teddy.obj");
  drawDatas[3] = loadTexture("teapot.obj");
  drawDatas[4] = loadTexture("cow.obj");
  drawDatas[5] = loadTexture("bunny.obj");
}

// Clear frame buffer
void clearBuffer()
{
  Color clearColor = {0.0, 0.0, 0.0};   // clear color: black
  for (int i=0; i<window_width*window_height; ++i)
  {
    pixels[i*3]   = clearColor.r;
    pixels[i*3+1] = clearColor.g;
    pixels[i*3+2] = clearColor.b;
  }  
}

// Draw a point into the frame buffer
void drawPoint(int x, int y, float r, float g, float b)
{
  int offset = y*window_width*3 + x*3;
  if (offset > window_width * window_height * 3 || offset < 0) return;
  pixels[offset]   = r;
  pixels[offset+1] = g;
  pixels[offset+2] = b;
}

void rasterizeRegion(Vector4 v1, Vector3 c1, Vector4 v2, Vector3 c2, Vector4 v3, Vector3 c3, double xMin, double xMax, double yMin, double yMax)
{
  double x1 = v1[0];
  double y1 = v1[1];
  double x2 = v2[0];
  double y2 = v2[1];
  double x3 = v3[0];
  double y3 = v3[1];
  
  for (int x = max(0, xMin); x < min(window_width, xMax); x++)
  {
    for (int y = max(0, yMin); y < min(window_height, yMax); y++)
    {
      double bary1 = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
      double bary2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
      double bary3 = 1 - bary1 - bary2;
      if (bary1 < 0 || bary1 > 1) continue;
      if (bary2 < 0 || bary2 > 1) continue;
      if (bary3 < 0 || bary3 > 1) continue;

      int xFloored = x;
      int yFloored = y;
      double zIndex = v1[2] * bary1 + v2[2] * bary2 + v3[2] * bary3;
      if (zIndex >= zBuffer[window_width * yFloored + xFloored]) continue;

      zBuffer[window_width * yFloored + xFloored] = zIndex;
      Vector3 color = c1.scale(bary1) + c2.scale(bary2) + c3.scale(bary3);
      drawPoint(xFloored, yFloored, color.x, color.y, color.z);
    }
  }

  if (boundaries)
  {
    for (int x = max(0, xMin); x < min(window_width, xMax); x++)
    {
      drawPoint(x, yMin, 1, 0, 0);
      drawPoint(x, yMax, 1, 0, 0);
    }

    for (int y = max(0, yMin); y < min(window_height, yMax); y++)
    {
      drawPoint(xMin, y, 1, 0, 0);
      drawPoint(xMax, y, 1, 0, 0);
    }
  }
}

struct point { double x, y; };
struct line { point s, e; };

int middle(int a, int b, int c) {
  int t;    
  if ( a > b ) {
    t = a;
    a = b;
    b = t;
  }
  if ( a <= c && c <= b ) return 1;
  return 0;
}

double CCW(point a, point b, point c)
{ return (b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x); }

bool intersect(line a, line b) {
  if ( ( CCW(a.s, a.e, b.s) * CCW(a.s, a.e, b.e) < 0 ) &&
     ( CCW(b.s, b.e, a.s) * CCW(b.s, b.e, a.e) < 0 ) ) return true;

  if ( CCW(a.s, a.e, b.s) == 0 && middle(a.s.x, a.e.x, b.s.x) && middle(a.s.y, a.e.y, b.s.y) ) return true;
  if ( CCW(a.s, a.e, b.e) == 0 && middle(a.s.x, a.e.x, b.e.x) && middle(a.s.y, a.e.y, b.e.y) ) return true;
  if ( CCW(b.s, b.e, a.s) == 0 && middle(b.s.x, b.e.x, a.s.x) && middle(b.s.y, b.e.y, a.s.y) ) return true;
  if ( CCW(b.s, b.e, a.e) == 0 && middle(b.s.x, b.e.x, a.e.x) && middle(b.s.y, b.e.y, a.e.y) ) return true;

    return false;
}

bool linesIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
{
  point a;
  a.x = x1;
  a.y = y1;
  point b;
  b.x = x2;
  b.y = y2;
  point c;
  c.x = x3;
  c.y = y3;
  point d;
  d.x = x4;
  d.y = y4;
  line e;
  e.s = a;
  e.e = b;
  line f;
  f.s = c;
  f.e = d;
  return intersect(e, f);
}

bool vertexInRect(Vector4 v, double xMin, double xMax, double yMin, double yMax)
{
  return v.x >= xMin && v.x < xMax && v.y >= yMin && v.y < yMax;
}

bool pointInTriangle(Vector4 v1, Vector4 v2, Vector4 v3, double x, double y)
{
  double x1 = v1[0];
  double y1 = v1[1];
  double x2 = v2[0];
  double y2 = v2[1];
  double x3 = v3[0];
  double y3 = v3[1];
  double bary1 = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
  double bary2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / ((y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3));
  double bary3 = 1 - bary1 - bary2;
  if (bary1 < 0 || bary1 > 1) return false;
  if (bary2 < 0 || bary2 > 1) return false;
  if (bary3 < 0 || bary3 > 1) return false;

  return true;
}

bool triangleRectIntersect(Vector4 v1, Vector4 v2, Vector4 v3, double xMin, double xMax, double yMin, double yMax)
{
  if (pointInTriangle(v1, v2, v3, xMin, yMin)) return true;
  if (pointInTriangle(v1, v2, v3, xMin, yMax)) return true;
  if (pointInTriangle(v1, v2, v3, xMax, yMin)) return true;
  if (pointInTriangle(v1, v2, v3, xMax, yMax)) return true;

  if (vertexInRect(v1, xMin, xMax, yMin, yMax)) return true;
  if (vertexInRect(v2, xMin, xMax, yMin, yMax)) return true;
  if (vertexInRect(v3, xMin, xMax, yMin, yMax)) return true;

  if (linesIntersect(v1.x, v1.y, v2.x, v2.y, xMin, yMin, xMin, yMax)) return true;
  if (linesIntersect(v2.x, v2.y, v3.x, v3.y, xMin, yMin, xMin, yMax)) return true;
  if (linesIntersect(v3.x, v3.y, v1.x, v1.y, xMin, yMin, xMin, yMax)) return true;

  if (linesIntersect(v1.x, v1.y, v2.x, v2.y, xMin, yMax, xMax, yMax)) return true;
  if (linesIntersect(v2.x, v2.y, v3.x, v3.y, xMin, yMax, xMax, yMax)) return true;
  if (linesIntersect(v3.x, v3.y, v1.x, v1.y, xMin, yMax, xMax, yMax)) return true;

  if (linesIntersect(v1.x, v1.y, v2.x, v2.y, xMax, yMin, xMax, yMax)) return true;
  if (linesIntersect(v2.x, v2.y, v3.x, v3.y, xMax, yMin, xMax, yMax)) return true;
  if (linesIntersect(v3.x, v3.y, v1.x, v1.y, xMax, yMin, xMax, yMax)) return true;

  if (linesIntersect(v1.x, v1.y, v2.x, v2.y, xMax, yMin, xMin, yMin)) return true;
  if (linesIntersect(v2.x, v2.y, v3.x, v3.y, xMax, yMin, xMin, yMin)) return true;
  if (linesIntersect(v3.x, v3.y, v1.x, v1.y, xMax, yMin, xMin, yMin)) return true;
  return false;
}

void rasterizeTriangle(Vector4 v1, Vector3 c1, Vector4 v2, Vector3 c2, Vector4 v3, Vector3 c3)
{
  double x1 = v1[0];
  double y1 = v1[1];
  double x2 = v2[0];
  double y2 = v2[1];
  double x3 = v3[0];
  double y3 = v3[1];

  double xMin = min(x1, min(x2, x3));
  double xMax = max(x1, max(x2, x3));
  double yMin = min(y1, min(y2, y3));
  double yMax = max(y1, max(y2, y3));

  if (optimize)
  {
    int n = 4;
    for (int x = 0; x < n; x++)
    {
      for (int y = 0; y < n; y++)
      {
        double xMinRegion = xMin + x * (xMax - xMin) / n;
        double xMaxRegion = xMin + (x + 1) * (xMax - xMin) / n;
        double yMinRegion = yMin + y * (yMax - yMin) / n;
        double yMaxRegion = yMin + (y + 1) * (yMax - yMin) / n;

        if (triangleRectIntersect(v1, v2, v3, xMinRegion, xMaxRegion, yMinRegion, yMaxRegion))
        {
          rasterizeRegion(v1, c1, v2, c2, v3, c3, xMinRegion, xMaxRegion, yMinRegion, yMaxRegion);
        }
      }
    }
  }
  else
  {
    rasterizeRegion(v1, c1, v2, c2, v3, c3, xMin, xMax, yMin, yMax);
  }
}

Vector4 rasterizeVertex(Vector4 p)
{
  Matrix4 projection = MatrixUtils::createPerspective(window_width / window_height, PI / 2, 10, 1000);
  Matrix4 projectTranslate = Matrix4::translation(0, 0, -20);
  Matrix4 viewport = MatrixUtils::createViewport(window_width, window_height);
  Matrix4 matrix = viewport * projection * projectTranslate * camera * model;
  return (matrix * p).dehomogenize();
}

void drawHouseVertices()
{
  for (int i = 0; i < House::nVerts; i++)
  {
    Vector4 location = Vector4(House::vertices[i * 3], House::vertices[i * 3 + 1], House::vertices[i * 3 + 2]);
    Vector4 imageCoords = rasterizeVertex(location);
    drawPoint(imageCoords[0], imageCoords[1], 1, 1, 1);
  }
}

void drawHouse()
{
  if (drawMode == 7)
  {
    camera = MatrixUtils::createCamera(Vector3(0, 10, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
  }
  else
  {
    camera = MatrixUtils::createCamera(Vector3(-15, 5, 10), Vector3(-5, 0, 0), Vector3(0, 1, 0.5));
  }

  for (int i = 0; i < House::indicesLength; i += 3)
  {
    int idx1 = House::indices[i];
    int idx2 = House::indices[i + 1];
    int idx3 = House::indices[i + 2];

    Vector4 location1 = Vector4(House::vertices[idx1 * 3], House::vertices[idx1 * 3 + 1], House::vertices[idx1 * 3 + 2]);
    Vector4 imageCoords1 = rasterizeVertex(location1);
    Vector3 color1 = Vector3(House::colors[idx1 * 3], House::colors[idx1 * 3 + 1], House::colors[idx1 * 3 + 2]);

    Vector4 location2 = Vector4(House::vertices[idx2 * 3], House::vertices[idx2 * 3 + 1], House::vertices[idx2 * 3 + 2]);
    Vector4 imageCoords2 = rasterizeVertex(location2);
    Vector3 color2 = Vector3(House::colors[idx2 * 3], House::colors[idx2 * 3 + 1], House::colors[idx2 * 3 + 2]);

    Vector4 location3 = Vector4(House::vertices[idx3 * 3], House::vertices[idx3 * 3 + 1], House::vertices[idx3 * 3 + 2]);
    Vector4 imageCoords3 = rasterizeVertex(location3);
    Vector3 color3 = Vector3(House::colors[idx3 * 3], House::colors[idx3 * 3 + 1], House::colors[idx3 * 3 + 2]);

    rasterizeTriangle(imageCoords1, color1, imageCoords2, color2, imageCoords3, color3);
  }
}

void drawTexture()
{
  DrawData drawData = drawDatas[drawMode];
  for (int i = 0; i < drawData.nIndices; i += 3)
  {
    Vector3 color;
    if (i % 2 == 0)
    {
      color = Vector3(1, 0, 0);
    }
    else
    {
      color = Vector3(0, 0, 1);
    }

    int idx1 = drawData.indices[i];
    int idx2 = drawData.indices[i + 1];
    int idx3 = drawData.indices[i + 2];

    Vector4 location1 = Vector4(drawData.vertices[idx1 * 3], drawData.vertices[idx1 * 3 + 1], drawData.vertices[idx1 * 3 + 2]);
    Vector4 imageCoords1 = rasterizeVertex(location1);

    Vector4 location2 = Vector4(drawData.vertices[idx2 * 3], drawData.vertices[idx2 * 3 + 1], drawData.vertices[idx2 * 3 + 2]);
    Vector4 imageCoords2 = rasterizeVertex(location2);

    Vector4 location3 = Vector4(drawData.vertices[idx3 * 3], drawData.vertices[idx3 * 3 + 1], drawData.vertices[idx3 * 3 + 2]);
    Vector4 imageCoords3 = rasterizeVertex(location3);

    rasterizeTriangle(imageCoords1, color, imageCoords2, color, imageCoords3, color);
  }
}

void rasterize()
{
  zBuffer = new double[window_width * window_height];
  for (int x = 0; x < window_width; x++)
  {
    for (int y = 0; y < window_height; y++)
    {
      zBuffer[window_width * y + x] = DBL_MAX;
    }
  }

  if (drawMode == 7 || drawMode == 8)
  {
    model.identity();
    drawHouse();
  }
  else
  {
    angle += angularVelocity;
    model = Matrix4::zAxisRotation(zAngle) *
      Matrix4::translation(position) *
      Matrix4::yAxisRotation(angle) *
      Matrix4::scaling(scaleFactor, scaleFactor, scaleFactor);
    drawTexture();
  }

  delete[] zBuffer;
}

// Called whenever the window size changes
void reshape(int new_width, int new_height)
{
  window_width  = new_width;
  window_height = new_height;
  delete[] pixels;
  pixels = new float[window_width * window_height * 3];
}

void keyboard(unsigned char key, int mouseX, int mouseY)
{
  cout << "key pressed" << endl;
  switch (key)
  {
    case 'c':
      angularVelocity = -angularVelocity;
      break;
    case 'x':
      position += Vector3(-speed, 0, 0);
      break;
    case 'X':
      position += Vector3(speed, 0, 0);
      break;
    case 'y':
      position += Vector3(0, -speed, 0);
      break;
    case 'Y':
      position += Vector3(0, speed, 0);
      break;
    case 'z':
      position += Vector3(0, 0, -speed);
      break;
    case 'Z':
      position += Vector3(0, 0, speed);
      break;
    case 'a':
      zAngle += .1;
      break;
    case 'A':
      zAngle += -.1;
      break;
    case 's':
      scaleFactor *= .9;
      break;
    case 'S':
      scaleFactor *= 1.1;
      break;
    case 'r':
      position = Vector3();
      scaleFactor = 1;
      zAngle = 0;
      break;
    case 'b':
      boundaries = !boundaries;
      break;
    case 'o':
      optimize = !optimize;
      break;
  }
}

void specialCallback(int key, int x, int y)
{
  switch (key)
  {
    case GLUT_KEY_F1:
      drawMode = 0;
      break;
    case GLUT_KEY_F2:
      drawMode = 1;
      break;
    case GLUT_KEY_F3:
      drawMode = 2;
      break;
    case GLUT_KEY_F4:
      drawMode = 3;
      break;
    case GLUT_KEY_F5:
      drawMode = 4;
      break;
    case GLUT_KEY_F6:
      drawMode = 5;
      break;
    case GLUT_KEY_F8:
      drawMode = 7;
      break;
    case GLUT_KEY_F9:
      drawMode = 8;
      break;
  }
}

void display()
{
  clearBuffer();

  double start = clock();
  rasterize();
  double end = clock();
  cout << (end - start) / CLOCKS_PER_SEC * 1000 << endl;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // glDrawPixels writes a block of pixels to the framebuffer
  glDrawPixels(window_width, window_height, GL_RGB, GL_FLOAT, pixels);

  glutSwapBuffers();
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(window_width, window_height);
  glutCreateWindow("CSE 167 Project 3");

  loadData();

  glutReshapeFunc(reshape);
  glutIdleFunc(display);
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(specialCallback);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glutMainLoop();

  return 0;
}