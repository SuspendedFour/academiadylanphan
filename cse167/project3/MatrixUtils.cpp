#include <math.h>
#include "MatrixUtils.h"

Matrix4 MatrixUtils::createPerspective(double aspectRatio, double fieldOfViewRadians, double near, double far)
{
    Vector4 col0 = Vector4(1 / (aspectRatio * tan(fieldOfViewRadians / 2)), 0, 0, 0);
    Vector4 col1 = Vector4(0, 1 / tan(fieldOfViewRadians / 2), 0, 0);
    Vector4 col2 = Vector4(0, 0, (near + far) / (near - far), -1);
    Vector4 col3 = Vector4(0, 0, 2 * near * far / (near - far));
    return Matrix4(col0, col1, col2, col3);
}

Matrix4 MatrixUtils::createViewport(double width, double height)
{
    return Matrix4::translation(width / 2, height / 2, .5) * Matrix4::scaling(width / 2, height / 2, .5);
}

Matrix4 MatrixUtils::createCamera(Vector3 e, Vector3 d, Vector3 up)
{
    Vector3 z = (e - d).nor();
    Vector3 x = up.cross(z).nor();
    Vector3 y = z.cross(x);
    Matrix4 rotation = Matrix4(x.toVector4Vector(), y.toVector4Vector(), z.toVector4Vector(), Vector4(0, 0, 0, 1)).transpose();
    Matrix4 translation = Matrix4::translation(e.neg());
    return rotation * translation;
}
