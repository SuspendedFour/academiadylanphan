#include <math.h>
#include <iostream>

#include "Matrix4.h"
#include "Vector3.h"

using namespace std;

Matrix4::Matrix4()
{
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            m[i][j] = 0;
        }
    }
}

Matrix4::Matrix4(Vector4 col0, Vector4 col1, Vector4 col2, Vector4 col3)
{
    for (int i = 0; i < 4; i++)
    {
        m[0][i] = col0[i];
        m[1][i] = col1[i];
        m[2][i] = col2[i];
        m[3][i] = col3[i];
    }
}

Matrix4::Matrix4(
    double m00, double m01, double m02, double m03,
    double m10, double m11, double m12, double m13,
    double m20, double m21, double m22, double m23,
    double m30, double m31, double m32, double m33 )
{
    m[0][0] = m00;
    m[1][0] = m01;
    m[2][0] = m02;
    m[3][0] = m03;
    m[0][1] = m10;
    m[1][1] = m11;
    m[2][1] = m12;
    m[3][1] = m13;
    m[0][2] = m20;
    m[1][2] = m21;
    m[2][2] = m22;
    m[3][2] = m23;
    m[0][3] = m30;
    m[1][3] = m31;
    m[2][3] = m32;
    m[3][3] = m33;
}

Matrix4::Matrix4(const Matrix4 &matrix)
{
    m[0][0] = matrix.m[0][0];
    m[1][0] = matrix.m[1][0];
    m[2][0] = matrix.m[2][0];
    m[3][0] = matrix.m[3][0];
    m[0][1] = matrix.m[0][1];
    m[1][1] = matrix.m[1][1];
    m[2][1] = matrix.m[2][1];
    m[3][1] = matrix.m[3][1];
    m[0][2] = matrix.m[0][2];
    m[1][2] = matrix.m[1][2];
    m[2][2] = matrix.m[2][2];
    m[3][2] = matrix.m[3][2];
    m[0][3] = matrix.m[0][3];
    m[1][3] = matrix.m[1][3];
    m[2][3] = matrix.m[2][3];
    m[3][3] = matrix.m[3][3];
}

double* Matrix4::getPointer()
{
    return &m[0][0];
}

void Matrix4::identity()
{
    double ident[4][4]={{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};
    for (int i=0; i<4; ++i)
    {
        for (int j=0; j<4; ++j)
        {
            m[i][j] = ident[i][j];
        }
    }
}

// angle in radians
void Matrix4::rotateY(double angle)
{
    m[0][0] = cos(angle);
    m[1][0] = 0;
    m[2][0] = sin(angle);
    m[3][0] = 0;
    m[0][1] = 0;
    m[1][1] = 1;
    m[2][1] = 0;
    m[3][1] = 0;
    m[0][2] = -sin(angle);
    m[1][2] = 0;
    m[2][2] = cos(angle);
    m[3][2] = 0;
    m[0][3] = 0;
    m[1][3] = 0;
    m[2][3] = 0;
    m[3][3] = 1;
}

double Matrix4::get(int row, int column) const
{
    return m[column][row];
}

void Matrix4::set(int row, int column, double value)
{
    m[column][row] = value;
}

Matrix4 Matrix4::mul(const Matrix4& other) const
{
    Matrix4 result = Matrix4();
    for (int row = 0; row < 4; row++)
    {
        for (int col = 0; col < 4; col++)
        {
            double sum = 0;
            for (int i = 0; i < 4; i++)
            {
                sum += m[i][row] * other.m[col][i];
            }

            result.set(row, col, sum);
        }
    }

	return result;
}

Matrix4 Matrix4::operator*(const Matrix4& other) const
{
    return mul(other);
}

Vector4 Matrix4::mul(const Vector4& v) const
{
    double x = 0;
    double y = 0;
    double z = 0;
    double w = 0;

    for (int i = 0; i < 4; i++)
    {
        x += m[i][0] * v[i];
        y += m[i][1] * v[i];
        z += m[i][2] * v[i];
        w += m[i][3] * v[i];
    }

    return Vector4(x, y, z, w);
}

Vector4 Matrix4::operator*(const Vector4& other) const
{
    return mul(other);
}

Matrix4 Matrix4::xAxisRotation(double radians)
{
    Matrix4 result = Matrix4();
    result.m[0][0] = 1;
    result.m[1][1] = cos(radians);
    result.m[1][2] = sin(radians);
    result.m[2][1] = -sin(radians);
    result.m[2][2] = cos(radians);
    result.m[3][3] = 1;
    return result;
}

Matrix4 Matrix4::yAxisRotation(double radians)
{
    Matrix4 result = Matrix4();
    result.m[0][0] = cos(radians);
    result.m[2][0] = sin(radians);
    result.m[1][1] = 1;
    result.m[0][2] = -sin(radians);
    result.m[2][2] = cos(radians);
    result.m[3][3] = 1;
    return result;
}

Matrix4 Matrix4::zAxisRotation(double radians)
{
    Matrix4 result = Matrix4();
    result.m[0][0] = cos(radians);
    result.m[0][1] = sin(radians);
    result.m[1][0] = -sin(radians);
    result.m[1][1] = cos(radians);
    result.m[2][2] = 1;
    result.m[3][3] = 1;
    return result;
}

Matrix4 Matrix4::rotation(double radians, Vector3 axis)
{
    Matrix4 result = Matrix4();
    Vector3 unitAxis = axis.nor();
    double ax = unitAxis.getX();
    double ay = unitAxis.getY();
    double az = unitAxis.getZ();

    result.m[0][0] = 1 + (1 - cos(radians)) * (ax * ax - 1);
    result.m[1][0] = -az * sin(radians) + (1 - cos(radians)) * ax * ay;
    result.m[2][0] = ay * sin(radians) + (1 - cos(radians)) * ax * az;
    result.m[3][0] = 0;
    result.m[0][1] = az * sin(radians) + (1 - cos(radians)) * ay * ax;
    result.m[1][1] = 1 + (1 - cos(radians)) * (ay * ay - 1);
    result.m[2][1] = -ax * sin(radians) + (1 - cos(radians)) * ay *  az;
    result.m[3][1] = 0;
    result.m[0][2] = -ay * sin(radians) + (1 - cos(radians)) * az * ax;
    result.m[1][2] = ax * sin(radians) + (1 - cos(radians)) * az * ay;
    result.m[2][2] = 1 + (1 - cos(radians)) * (az * az - 1);
    result.m[3][2] = 0;
    result.m[0][3] = 0;
    result.m[1][3] = 0;
    result.m[2][3] = 0;
    result.m[3][3] = 1;
    return result;
}

Matrix4 Matrix4::scaling(double xFactor, double yFactor, double zFactor)
{
    Matrix4 result = Matrix4();
    result.m[0][0] = xFactor;
    result.m[1][1] = yFactor;
    result.m[2][2] = zFactor;
    result.m[3][3] = 1;
    return result;
}

Matrix4 Matrix4::translation(double x, double y, double z)
{
    Matrix4 result = Matrix4();
    result.m[3][0] = x;
    result.m[3][1] = y;
    result.m[3][2] = z;
    result.m[0][0] = 1;
    result.m[1][1] = 1;
    result.m[2][2] = 1;
    result.m[3][3] = 1;

    return result;
}

Matrix4 Matrix4::translation(Vector3 t)
{
    return translation(t.getX(), t.getY(), t.getZ());
}

void Matrix4::print() const
{
    cout << "[ [";
    for (int col = 0; col < 4; col++)
    {
        cout << " " << m[col][0];
    }

    cout << " ]";
    for (int row = 1; row < 4; row++)
    {
        cout << endl << "  [";
        for (int col = 0; col < 4; col++)
        {
            cout << " " << m[col][row];
        }

        cout << " ]";
    }

    cout << " ]" << endl;
}

Matrix4 Matrix4::transpose() const
{
    Matrix4 result = Matrix4();
    result.m[0][0] = m[0][0];
    result.m[1][0] = m[0][1];
    result.m[2][0] = m[0][2];
    result.m[3][0] = m[0][3];
    result.m[0][1] = m[1][0];
    result.m[1][1] = m[1][1];
    result.m[2][1] = m[1][2];
    result.m[3][1] = m[1][3];
    result.m[0][2] = m[2][0];
    result.m[1][2] = m[2][1];
    result.m[2][2] = m[2][2];
    result.m[3][2] = m[2][3];
    result.m[0][3] = m[3][0];
    result.m[1][3] = m[3][1];
    result.m[2][3] = m[3][2];
    result.m[3][3] = m[3][3];
    return result;
}

/** Rotates the matrix according to a fictitious trackball, placed in
    the middle of the given window.
    The trackball is approximated by a Gaussian curve.
    The trackball coordinate system is: x=right, y=up, z=to viewer<BR>
    The origin of the mouse coordinates zero (0,0) is considered to be top left.
  @param width, height  window size in pixels
  @param fromX, fromY   mouse starting position in pixels
  @param toX, toY       mouse end position in pixels
*/
Matrix4 Matrix4::trackballRotation(int width, int height, int fromX, int fromY, int toX, int toY) const
{
    const float TRACKBALL_SIZE = 1.3f;              // virtual trackball size (empirical value)
    Vector3 v1, v2;                                 // mouse drag positions in normalized 3D space
    float smallSize;                                // smaller window size between width and height
    float halfWidth, halfHeight;                    // half window sizes
    float angle;                                    // rotational angle
    float d;                                        // distance

    // Compute mouse coordinates in window and normalized to -1..1
    // ((0,0)=window center, (-1,-1) = bottom left, (1,1) = top right)
    halfWidth   = (float)width  / 2.0f;
    halfHeight  = (float)height / 2.0f;
    smallSize   = (halfWidth < halfHeight) ? halfWidth : halfHeight;
    v1[0]       = ((float)fromX - halfWidth)  / smallSize;
    v1[1]       = ((float)(height-fromY) - halfHeight) / smallSize;
    v2[0]       = ((float)toX   - halfWidth)  / smallSize;
    v2[1]       = ((float)(height-toY)   - halfHeight) / smallSize;

    // Compute z-coordinates on Gaussian trackball:
    d       = sqrtf(v1[0] * v1[0] + v1[1] * v1[1]);
    v1[2]   = expf(-TRACKBALL_SIZE * d * d);
    d       = sqrtf(v2[0] * v2[0] + v2[1] * v2[1]);
    v2[2]   = expf(-TRACKBALL_SIZE * d * d);

    // Compute rotational angle:
    angle = v1.angle(v2);                          // angle = angle between v1 and v2

    // Compute rotational axis:
    v2 = v2.cross(v1);

    // Convert axis coordinates (v2) from WCS to OCS:
    Matrix4 mInv = extractRotation().transpose();
	v2 = (mInv * v2.toVector4Vector()).toVector3().nor();

    // Perform acutal model view matrix modification:
    return Matrix4::rotation(-angle, Vector3(v2[0], v2[1], v2[2]));      // rotate model view matrix
}

Matrix4 Matrix4::extractRotation() const
{
    Matrix4 result = Matrix4(*this);
    for (int i = 0; i < 3; i++)
    {
        result.m[3][i] = 0;
        result.m[i][3] = 0;
    }

    return result;
}
