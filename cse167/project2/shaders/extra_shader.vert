varying vec3 normal, lightDir;
uniform int clk;
uniform int viewportheight;
uniform int reflection;

float rand(vec2 co)
{
  return fract(sin(dot(co.xy,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{   
    // Note that gl_LightSource, gl_NormalMatrix, and gl_Normal
    // are pre-defined variables that access the current OpenGL
    // state.
    lightDir = normalize(vec3(gl_LightSource[0].position));
    normal = normalize(gl_NormalMatrix * gl_Normal);

    float c = rand(vec2(gl_Vertex.x / gl_Vertex.z, gl_Vertex.y + gl_Vertex.z));
    gl_FrontColor = vec4(c, c, c, 1);
    
    // ftransform() is a built-in function that applies all
    // transformations (i.e., modelview and 
    // projection) to a vertex.
    // gl_Position = ftransform() + sin(gl_Vertex.x * gl_Vertex.y * gl_Vertex.z) * rand(vec2(clk, clk));
    if (reflection == 1)
    {
        gl_Position = ftransform() + sin(ftransform().x * ftransform().y) * rand(vec2(clk, clk));
        gl_Position.y = -ftransform().y - 10;
    }
    else
    {
        gl_Position = ftransform();
        gl_Position.y = gl_Position.y + 10;
    }
}