varying vec4 diffuseA,ambientA;
varying vec3 normal,halfVectorA;

varying vec4 diffuseB,ambientGlobalB, ambientB, ecPosB;
varying vec3 halfVectorB;

varying vec4 diffuseC,ambientGlobalC, ambientC, ecPosC;
varying vec3 halfVectorC;

uniform int light0Enabled;
uniform int light1Enabled;
uniform int light2Enabled;
uniform int effectsEnabled;
 
 vec4 fun1()
 {
    vec3 n,halfV,lightDir;
    float NdotL,NdotHV;
 
    lightDir = vec3(gl_LightSource[0].position);
 
    /* The ambientA term will always be present */
    vec4 color = ambientA;
    /* a fragment shader can't write a varying variable, hence we need
    a new variable to store the normalized interpolated normal */
    n = normalize(normal);
    /* compute the dot product between normal and ldir */
 
    NdotL = max(dot(n,lightDir),0.0);

    if (NdotL > 0.0) {
        color += diffuseA * NdotL;
        halfV = normalize(halfVectorA);
        NdotHV = max(dot(n,halfV),0.0);
        color += gl_FrontMaterial.specular *
                gl_LightSource[0].specular *
                pow(NdotHV, gl_FrontMaterial.shininess);
    }
 
    return color;
}

vec4 fun2()
{
    vec3 n,halfV,viewV,lightDir;
    float NdotL,NdotHV;
    vec4 color = ambientGlobalB;
    float att, dist;
     
    /* a fragment shader can't write a verying variable, hence we need
    a new variable to store the normalized interpolated normal */
    n = normalize(normal);
     
    // Compute the ligt direction
    lightDir = vec3(gl_LightSource[1].position-ecPosB);
     
    /* compute the distance to the light source to a varying variable*/
    dist = length(lightDir);
    
     
    /* compute the dot product between normal and ldir */
    NdotL = max(dot(n,normalize(lightDir)),0.0);
    
    if (NdotL > 0.0) {
     
        att = 1.0 / (gl_LightSource[1].constantAttenuation +
                gl_LightSource[1].linearAttenuation * dist +
                gl_LightSource[1].quadraticAttenuation * dist * dist);
        color += att * (diffuseB * NdotL + ambientB);
     
         
        halfV = normalize(halfVectorB);
        NdotHV = max(dot(n,halfV),0.0);
        color += att * gl_FrontMaterial.specular * gl_LightSource[1].specular * pow(NdotHV,gl_FrontMaterial.shininess);
    }

    // if (NdotHV > 1 && effectsEnabled == 1)
    if (effectsEnabled == 1 && dot(lightDir, normal) < 1.0)
    {
        color.x = color.x * 2.0;
    }

    return color;
}

vec4 fun3()
{
    vec3 n,halfV,lightDir;
    float NdotL,NdotHV;
    vec4 color = ambientGlobalC;
    float att,spotEffect,dist;
     
    /* a fragment shader can't write a verying variable, hence we need
    a new variable to store the normalized interpolated normal */
    n = normalize(normal);
     
    // Compute the ligt direction
    lightDir = vec3(gl_LightSource[2].position-ecPosC);
     
    /* compute the distance to the light source to a varying variable*/
    dist = length(lightDir);

    /* compute the dot product between normal and ldir */
    NdotL = max(dot(n,normalize(lightDir)),0.0);

    if (NdotL > 0.0) {
     
        spotEffect = dot(normalize(gl_LightSource[2].spotDirection), normalize(-lightDir));
        if (spotEffect > gl_LightSource[2].spotCosCutoff) {
            spotEffect = pow(spotEffect, gl_LightSource[2].spotExponent);
            att = spotEffect / (gl_LightSource[2].constantAttenuation +
                    gl_LightSource[2].linearAttenuation * dist +
                    gl_LightSource[2].quadraticAttenuation * dist * dist);
                 
            color += att * (diffuseC * NdotL + ambientC);
         
             
            halfV = normalize(halfVectorC);
            NdotHV = max(dot(n,halfV),0.0);
            color += att * gl_FrontMaterial.specular * gl_LightSource[2].specular * pow(NdotHV,gl_FrontMaterial.shininess);
        }
    }

    return color;
}

void main()
{
    vec4 color = vec4(0, 0, 0, 0);
    if (light0Enabled == 1)
    {
        color = color + fun1();
    }
    
    if (light1Enabled == 1)
    {
        color = color + fun2();
    }

    if (light2Enabled == 1)
    {
        color = color + fun3();
    }

    gl_FragColor = color / vec4(4, 4, 4, 4);
    // gl_FragColor = (fun1() + fun2() + fun3() / vec4(3, 3, 3, 3));
}