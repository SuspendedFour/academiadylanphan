varying vec4 diffuseA,ambientA;
varying vec3 normal,halfVectorA;

varying vec4 diffuseB,ambientGlobalB,ambientB, ecPosB;
varying vec3 halfVectorB;

varying vec4 diffuseC,ambientGlobalC, ambientC, ecPosC;
varying vec3 halfVectorC;

uniform int light0Enabled;
uniform int light1Enabled;
uniform int light2Enabled;
uniform int effectsEnabled;

void fun1()
{
	/* first transform the normal into eye space and
	normalize the result */
	normal = normalize(gl_NormalMatrix * gl_Normal);
	
	/* pass the halfVectorA to the fragment shader */
	halfVectorA = gl_LightSource[0].halfVector.xyz;
	
	/* Compute the diffuseA, ambientA and globalAmbient terms */
	diffuseA = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	ambientA = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	ambientA += gl_LightModel.ambient * gl_FrontMaterial.ambient;
	gl_Position = ftransform();
}

void fun2()
{
	vec3 aux;
	 
	/* first transform the normal into eye space and normalize the result */
	normal = normalize(gl_NormalMatrix * gl_Normal);
	
	/* compute the vertex position  in camera space. */
	ecPosB = gl_ModelViewMatrix * gl_Vertex;
	
	/* Normalize the halfVectorB to pass it to the fragment shader */
	halfVectorB = gl_LightSource[1].halfVector.xyz;
	 
	/* Compute the diffuseB, ambientB and globalAmbient terms */
	diffuseB = gl_FrontMaterial.diffuse * gl_LightSource[1].diffuse;
	ambientB = gl_FrontMaterial.ambient * gl_LightSource[1].ambient;
	ambientGlobalB = gl_LightModel.ambient * gl_FrontMaterial.ambient;
}
 

void fun3()
{
	vec3 aux;
	 
	/* first transform the normal into eye space and normalize the result */
	normal = normalize(gl_NormalMatrix * gl_Normal);

	/* compute the vertex position  in camera space. */
	ecPosC = gl_ModelViewMatrix * gl_Vertex;

	/* Normalize the halfVectorC to pass it to the fragment shader */
	halfVectorC = gl_LightSource[2].halfVector.xyz;
	 
	/* Compute the diffuseC, ambientC and globalAmbient terms */
	diffuseC = gl_FrontMaterial.diffuse * gl_LightSource[2].diffuse;
	ambientC = gl_FrontMaterial.ambient * gl_LightSource[2].ambient;
	ambientGlobalC = gl_LightModel.ambient * gl_FrontMaterial.ambient;
}

void main()
{
    fun1();
    fun2();
    fun3();

    gl_Position = ftransform();

    if (light0Enabled == 1 && effectsEnabled == 1)
    {
	    vec3 lightDir = normalize(vec3(gl_LightSource[0].position));
	    float angle_D = (dot(normal,normalize(lightDir))) / (  length(normal) * length(normalize(lightDir)));
		vec4 direct = angle_D * normalize(ftransform() - gl_LightSource[0].position);

	    if (dot(lightDir, normal) < 5.0)
	    {
			gl_Position = gl_Position + direct;
	    }
    }
}