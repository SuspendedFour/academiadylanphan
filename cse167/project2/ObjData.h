#ifndef OBJ_DATA_H
#define OBJ_DATA_H

using namespace std;

class ObjData
{
public:
    float* vertices;
    int nVertices;
    int nIndices;
    int* indices;
    float* normals;
    float* colors;
    Matrix4 normalization;

	void computeNormalization(int width, int height)
	{
	  float minX = vertices[0];
	  float maxX = vertices[0];
	  float minY = vertices[1];
	  float maxY = vertices[1];
	  float minZ = vertices[2];
	  float maxZ = vertices[2];

	  for (int i = 0; i < nVertices; i++)
	  {
		minX = min(minX, vertices[i * 3]);
		maxX = max(maxX, vertices[i * 3]);
		minY = min(minY, vertices[i * 3 + 1]);
		maxY = max(maxY, vertices[i * 3 + 1]);
		minZ = min(minZ, vertices[i * 3 + 2]);
		maxZ = max(maxZ, vertices[i * 3 + 2]);
	  }

	  float centerX = minX + (maxX - minX) / 2;
	  float centerY = minY + (maxY - minY) / 2;
	  float centerZ = minZ + (maxZ - minZ) / 2;

	  float scale = 1 / max(maxX - centerX, max(maxY - centerY, maxZ - centerZ));
	  scale *= min(width, height) / 35;

	  normalization = Matrix4::scaling(scale, scale, scale) * Matrix4::translation(-centerX, -centerY, -centerZ);
	};

    void generateColors()
    {
        colors = new float[nVertices * 3];
		for (int i = 0; i < nVertices * 3; i++)
        {
            colors[i] = (float) (rand() % 100) / 100;
        }
    }
};

#endif