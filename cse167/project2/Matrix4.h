#ifndef _MATRIX4_H_
#define _MATRIX4_H_

#include "Vector4.h"
#include "Vector3.h"

class Matrix4
{
    private:
        double m[4][4];

    public:
        Matrix4();
        Matrix4(const Matrix4&);
        Matrix4(Vector4, Vector4, Vector4, Vector4);
        Matrix4(double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double);
        double* getPointer();  // return pointer to matrix elements
        void identity();  // create identity matrix
        void rotateY(double); // rotation about y axis

        double get(int, int) const;
        void set(int, int, double);
        Matrix4 mul(const Matrix4&) const;
        Matrix4 operator*(const Matrix4&) const;
        Vector4 mul(const Vector4&) const;
        Vector4 operator*(const Vector4&) const;
        static Matrix4 xAxisRotation(double);
        static Matrix4 yAxisRotation(double);
        static Matrix4 zAxisRotation(double);
        static Matrix4 rotation(double, Vector3);
        static Matrix4 scaling(double, double, double);
        static Matrix4 translation(double, double, double);
        static Matrix4 translation(Vector3);
        void print() const;
        Matrix4 transpose() const;
        Matrix4 trackballRotation(int width, int height, int fromX, int fromY, int toX, int toY) const;
        Matrix4 extractRotation() const;
};

#endif