#ifndef LIGHT_H
#define LIGHT_H
#define PI 3.141592653589793238462

#include <GL/glut.h>


class Light
{
public:
    float lightId;
    float* color;
    float* position;
    float* spotDirection;
    float spotCutoff;
    float attenuation;
    bool enabled;

	Light() { enabled = false; }

    void render()
    {
        if (enabled)
        {
            glLightfv(lightId, GL_POSITION, position);
            glLightfv(lightId, GL_DIFFUSE, color);
            float specular[] = {1, 1, 1, 1};
            glLightfv(lightId, GL_SPECULAR, specular);

            if (spotDirection)
            {
                glLightfv(lightId, GL_SPOT_DIRECTION, spotDirection);
            }
            if (spotCutoff)
            {
                glLightf(lightId, GL_SPOT_CUTOFF, spotCutoff);
            }

            if (attenuation)
            {
                glLightf(lightId, GL_LINEAR_ATTENUATION, attenuation);
            }

            glEnable(lightId);
        }
        else
        {
            glDisable(lightId);
        }
    }
};

#endif