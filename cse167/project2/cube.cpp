#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "glee.h"
#include <GL/glut.h>
#include "Matrix4.h"
#include "House.h"
#include "objreader.h"
#include "ObjData.h"
#include "Light.h"
#include "Material.h"
#include "shader.h"
#include <ctime>

#define PI 3.141592653589793238462

using namespace std;

static float diffuse0[] = {.2, .4, .8, 1};
static float ambient0[] = {.8, .6, .2, 1};
static float specular0[] = {1, 1, 1, 1};

static float diffuse1[] = {1, 0, 0, 1};
static float ambient1[] = {0, 1, 1, 1};
static float specular1[] = {1, 1, 1, 1};

static float diffuse2[] = {0, 1, 0, 1};
static float ambient2[] = {1, 0, 1, 1};
static float specular2[] = {0, 0, 0, 1};

static float diffuse3[] = {0, 0, 1, 1};
static float ambient3[] = {1, 1, 0, 1};
static float specular3[] = {0, 0, 0, 1};

static Matrix4 objectTransform;
static Matrix4 lightTransform;

static ObjData objDatas[5];
static Material materials[5];
static int drawMode = 0;

static int mouseX;
static int mouseY;
static int mouseButton;

static Light directionalLight;
static Light pointLight;
static Light spotLight;
static bool freezeActive = false;

static bool shaderEnabled = false;
static bool effectsEnabled = false;
static bool extraScene = false;

static Shader* shader;
static Shader* extraShader;
// static Shader shader = Shader("shaders/simple.vert", "shaders/simple.frag");

int width  = 512;   // set window width in pixels here
int height = 512;   // set window height in pixels here

ObjData loadTexture(char* filename)
{
  ObjData result;
  float* texcoords;
  ObjReader::readObj(filename, result.nVertices, &result.vertices, &result.normals, &texcoords, result.nIndices, &result.indices);
  result.computeNormalization(width, height);
  result.generateColors();
  return result;
}

void loadTextures()
{
  objDatas[0] = loadTexture("cube.obj");
  objDatas[1] = loadTexture("dragon_smooth.obj");
  objDatas[2] = loadTexture("bunny_n.obj");
  objDatas[3] = loadTexture("sandal.obj");
  // objDatas[4] = loadTexture("sphere.obj");

  objectTransform = objDatas[drawMode].normalization;
  lightTransform = objectTransform;

  materials[0] = Material();
  materials[1] = Material();
  materials[2] = Material();
  materials[3] = Material();

  materials[0].diffuse = diffuse0;
  materials[0].ambient = ambient0;
  materials[0].specular = specular0;

  materials[1].diffuse = diffuse1;
  materials[1].ambient = ambient1;
  materials[1].specular = specular1;

  materials[2].diffuse = diffuse2;
  materials[2].ambient = ambient2;
  materials[2].specular = specular2;

  materials[3].diffuse = diffuse3;
  materials[3].ambient = ambient3;
  materials[3].specular = specular3;
}

//----------------------------------------------------------------------------
// Callback method called when window is resized.
void reshapeCallback(int w, int h)
{
  width = w;
  height = h;
  glViewport(0, 0, w, h);  // set new viewport size

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-10.0, 10.0, -10.0, 10.0, 10, 1000.0);
  glTranslatef(0, 0, -20);
}

void drawTexture()
{
  glBegin(GL_TRIANGLES);

  Material material = materials[drawMode];
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.diffuse);
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material.ambient);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.specular);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100);

  ObjData objData = objDatas[drawMode];
  for (int i = 0; i < objData.nIndices; i++)
  {
    int idx = objData.indices[i];
    glNormal3f(objData.normals[idx * 3], objData.normals[idx * 3 + 1], objData.normals[idx * 3 + 2]);
    glVertex3f(objData.vertices[idx * 3], objData.vertices[idx * 3 + 1], objData.vertices[idx * 3 + 2]);
  }

  glEnd();
}

static int clk = 0;
static int clk2 = 0;

//----------------------------------------------------------------------------
// Callback method called when window readraw is necessary or
// when glutPostRedisplay() was called.
void displayCallback(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers
  glMatrixMode(GL_MODELVIEW);

  if (extraScene)
  {
    if (clk2 % 30 == 0) {
      clk++;
    }

    clk2++;
    int i = glGetUniformLocation(extraShader->getPid(), "clk");
    glUniform1i(i, clk);

    int i2 = glGetUniformLocation(extraShader->getPid(), "viewportheight");
    glUniform1i(i2, height);

    int i3 = glGetUniformLocation(extraShader->getPid(), "reflection");
    glUniform1i(i3, 0);

    glLoadMatrixd(lightTransform.getPointer());
    directionalLight.render();
    pointLight.render();
    spotLight.render();

    // glLoadMatrixd((Matrix4::zAxisRotation(PI / 2) * objectTransform).getPointer());
    drawTexture();

    glUniform1i(i3, 1);
    drawTexture();
  }
  else
  {
    glLoadMatrixd(lightTransform.getPointer());
    directionalLight.render();
    pointLight.render();
    spotLight.render();

    glLoadMatrixd(objectTransform.getPointer());
    drawTexture();
  }

  glFlush();
  glutSwapBuffers();
}

void keyboardCallback(unsigned char key, int x, int y)
{
  int i;
  switch (key)
  {
    case '1':
      directionalLight.enabled = !directionalLight.enabled;
      i = glGetUniformLocation(shader->getPid(), "light0Enabled");
      glUniform1i(i, directionalLight.enabled);
      break;
    case '2':
      pointLight.enabled = !pointLight.enabled;
      i = glGetUniformLocation(shader->getPid(), "light1Enabled");
      glUniform1i(i, pointLight.enabled);
      break;
    case '3':
      spotLight.enabled = !spotLight.enabled;
      i = glGetUniformLocation(shader->getPid(), "light2Enabled");
      glUniform1i(i, spotLight.enabled);
      break;
    case 'm':
      freezeActive = !freezeActive;
      break;
    case 'q':
	   shaderEnabled = !shaderEnabled;
	   if (shaderEnabled)
	   {
		   shader->bind();
	   }
	   else
	   {
		   shader->unbind();
	   }

      // shader.printLog();
	  break;
    case 'w':
      effectsEnabled = !effectsEnabled;
      i = glGetUniformLocation(shader->getPid(), "effectsEnabled");
      glUniform1i(i, effectsEnabled);
      break;
    case 'e':
      extraScene = !extraScene;
      if (extraScene)
      {
        shaderEnabled = false;
        shader->unbind();
        extraShader->bind();
      }
      else
      {
        extraShader->unbind();
      }
      break;
  }
}

void specialCallback(int key, int x, int y)
{
  switch (key)
  {
    case GLUT_KEY_F1:
      drawMode = 0;
      objectTransform = objDatas[drawMode].normalization;
      lightTransform = objectTransform;
      break;
    case GLUT_KEY_F2:
      drawMode = 1;
      objectTransform = objDatas[drawMode].normalization;
      lightTransform = objectTransform;
      break;
    case GLUT_KEY_F3:
      drawMode = 2;
      objectTransform = objDatas[drawMode].normalization;
      lightTransform = objectTransform;
      break;
    case GLUT_KEY_F4:
      drawMode = 3;
      objectTransform = objDatas[drawMode].normalization;
      lightTransform = objectTransform;
      break;
    case GLUT_KEY_F5:
      // drawMode = 4;
      // objectTransform = objDatas[drawMode].normalization;
      // lightTransform = objectTransform;
      break;
  }
}

//----------------------------------------------------------------------------
// Callback method called when system is idle.
void idleCallback(void)
{
  displayCallback();  // call display routine to re-draw cube
}

void mouseCallback(int button, int state, int x, int y)
{
  if (state != GLUT_DOWN) return;
  mouseButton = button;
  mouseX = x;
  mouseY = y;
}

void motionCallback(int x, int y)
{
  if (x == mouseX && y == mouseY) return;
  if (mouseButton== GLUT_LEFT_BUTTON)
  {
    lightTransform = lightTransform * lightTransform.trackballRotation(width, height, mouseX, mouseY, x, y);
    if (!freezeActive)
    {
      objectTransform = objectTransform * objectTransform.trackballRotation(width, height, mouseX, mouseY, x, y);
    }
  }
  else if (mouseButton == GLUT_RIGHT_BUTTON)
  {
    double factor = 1 + .05 * (y < mouseY ? 1 : -1);
    lightTransform = Matrix4::scaling(factor, factor, factor) * lightTransform;
    if (!freezeActive)
    {
      objectTransform = Matrix4::scaling(factor, factor, factor) * objectTransform;
    }
  }

  mouseX = x;
  mouseY = y;
}

int main(int argc, char *argv[])
{
  float specular[]  = {1.0, 1.0, 1.0, 1.0};
  float shininess[] = {100.0};
  
  glutInit(&argc, argv);                      // initialize GLUT
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
  glutInitWindowSize(width, height);      // set initial window size
  glutCreateWindow("OpenGL Cube for CSE167");           // open window and set window title

  glEnable(GL_DEPTH_TEST);                    // enable depth buffering
  glClear(GL_DEPTH_BUFFER_BIT);               // clear depth buffer
  glClearColor(0.0, 0.0, 0.0, 0.0);           // set clear color to black
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
  glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
  glShadeModel(GL_SMOOTH);                    // set shading to smooth
  glMatrixMode(GL_PROJECTION); 
  glEnable(GL_NORMALIZE);
  
  // Generate material properties:
  // glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
  // glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  // Generate light source:
   glEnable(GL_LIGHTING);
  
  // Install callback functions:
  glutDisplayFunc(displayCallback);
  glutReshapeFunc(reshapeCallback);
  glutIdleFunc(idleCallback);
  glutKeyboardFunc(keyboardCallback);
  glutSpecialFunc(specialCallback);
  glutMouseFunc(mouseCallback);
  glutMotionFunc(motionCallback);

  float directionalLightPosition[]  = {0.0, 0.0, 100.0, 0.0};
  float directionalLightColor[] = {0, 1, 0, 1};
  // float directionalLightColor[] = {.2, 1, .2, 1};

  directionalLight.lightId = GL_LIGHT0;
  directionalLight.position = directionalLightPosition;
  directionalLight.color = directionalLightColor;

  float pointLightPosition[]  = {0.0, 100.0, 0.0, 1.0};
  // float pointLightColor[] = {1, 0, 0, 1};
  float pointLightColor[] = {1, .2, .2, 1};
  float pointLightAttenuation = .001;

  pointLight.lightId = GL_LIGHT1;
  pointLight.position = pointLightPosition;
  pointLight.color = pointLightColor;
  pointLight.attenuation = pointLightAttenuation;

  float spotLightPosition[]  = {200.0, 0.0, 0.0, 1.0};
  float spotLightSpotDirection[] = {-1, 0, 0};
  float spotLightColor[] = {0, 0, 1, 1};

  spotLight.lightId = GL_LIGHT2;
  spotLight.position = spotLightPosition;
  spotLight.spotDirection = spotLightSpotDirection;
  spotLight.color = spotLightColor;
  spotLight.spotCutoff = PI / 15;
  spotLight.attenuation = .001;

  loadTextures();

  shader = new Shader("shaders/diffuse_shading.vert", "shaders/diffuse_shading.frag");
  extraShader = new Shader("shaders/extra_shader.vert", "shaders/extra_shader.frag");

  glutMainLoop();

  return 0;
}

