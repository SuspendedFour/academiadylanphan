#ifndef MATERIAL_H
#define MATERIAL_H

class Material
{
public:
    float* diffuse;
    float* specular;
    float* ambient;
};

#endif