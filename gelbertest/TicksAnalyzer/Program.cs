﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TicksAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            TickerSummarizer summarizer = new TickerSummarizer(new TickerFileStrategy("ticks.txt"));
            List<TickerSummary> summaries = summarizer.Summarize();
            summaries.Sort((a, b) => a.TickerSymbol.CompareTo(b.TickerSymbol));
            IEnumerable<string> outputLines = summaries.Select((summary) => EncodeSummary(summary));
            File.WriteAllLines("output.txt", outputLines);
        }

        /// <summary>
        /// Converts a TickerSummary to string format.
        /// </summary>
        /// <param name="summary">The TickerSummary to convert.</param>
        /// <returns>The result of the conversion.</returns>
        static string EncodeSummary(TickerSummary summary)
        {
            string format = "{0,-6} VMAP:{1} BID:{2} ASK:{3} LAST:{4} SZ:{5} OPEN:{6} HIGH:{7} LOW:{8} VOL:{9} CNT:{10}";
            return string.Format(format,
                summary.TickerSymbol,
                summary.VolumeWeightedAveragePrice,
                summary.LastBid,
                summary.LastAsk,
                summary.LastPrice,
                summary.LastTradeSize,
                summary.OpenPrice,
                summary.HighPrice,
                summary.LowPrice,
                summary.Volume,
                summary.TotalTickCount);
        }
    }
}
