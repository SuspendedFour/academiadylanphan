﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicksAnalyzer
{
    class TickerSummarizer
    {
        private ITickerAccess tickerAccess;

        /// <param name="tickerAccess">The ticker feed to be summarized.</param>
        public TickerSummarizer(ITickerAccess tickerAccess)
        {
            this.tickerAccess = tickerAccess;
        }

        /// <summary>
        /// Produces a summary of the ticker feed for each ticker symbol.
        /// </summary>
        /// <returns></returns>
        public List<TickerSummary> Summarize()
        {
            List<TickerSummary> summaries = new List<TickerSummary>();
            var tickerGroups = tickerAccess.Ticks.GroupBy((tick) => tick.TickerSymbol);
            foreach (var tickerGroup in tickerGroups)
            {
                var group = tickerGroup.ToList();
                TickerSummary summary = new TickerSummary();
                summary.TickerSymbol = tickerGroup.Key;

                summary.Volume = group.Sum((tick) => tick.TradeSize);
                summary.VolumeWeightedAveragePrice = Math.Round(group.Sum((tick) => tick.TradePrice * tick.TradeSize) / summary.Volume, 2);

                summary.LastBid = group.Last().Bid;
                summary.LastAsk = group.Last().Ask;
                summary.LastPrice = group.Last().TradePrice;
                summary.LastTradeSize = group.Last().TradeSize;
                summary.OpenPrice = group.First().TradePrice;
                summary.HighPrice = group.Max((tick) => tick.TradePrice);
                summary.LowPrice = group.Max((tick) => tick.TradePrice);
                summary.TotalTickCount = group.Count();

                summaries.Add(summary);
            }

            return summaries;
        }
    }
}
