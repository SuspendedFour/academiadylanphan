﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TicksAnalyzer
{
    /// <summary>
    /// Reads ticks from a file.
    /// </summary>
    class TickerFileStrategy : ITickerAccess
    {
        const string START_OF_DAY = "09:30:00";
        const string END_OF_DAY = "04:00:00";
        string path;

        /// <param name="path">The path to the ticks file.</param>
        public TickerFileStrategy(string path)
        {
            this.path = path;
        }

        /// <summary>
        /// Accessor for the ticks. The ticks file is read each time this property is accessed.
        /// </summary>
        public IEnumerable<Tick> Ticks
        {
            get
            {
                string[] lines = File.ReadAllLines(path);
                return lines.Select((string line) => ParseTick(line)).Where(FilterTradingDay);
            }
        }

        /// <summary>
        /// Converts a string to Tick object.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <returns>The parsed Tick.</returns>
        private Tick ParseTick(string s)
        {
            string[] components = s.Split('|');

            Tick tick = new Tick();
            tick.TickerSymbol = components[0];
            tick.Time = components[1];
            tick.Bid = decimal.Parse(components[2]);
            tick.Ask = decimal.Parse(components[3]);
            tick.TradePrice = decimal.Parse(components[4]);
            tick.TradeSize = int.Parse(components[5]);
            return tick;
        }

        /// <summary>
        /// Determines if a Tick happened within the trading day.
        /// </summary>
        /// <param name="tick">The Tick to processs.</param>
        /// <returns>True if the Tick happened within the trading day, otherwise false.</returns>
        private bool FilterTradingDay(Tick tick)
        {
            return tick.Time.CompareTo(START_OF_DAY) >= 0 || tick.Time.CompareTo(END_OF_DAY) <= 0;
        }
    }
}
