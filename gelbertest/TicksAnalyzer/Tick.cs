﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicksAnalyzer
{
    /// <summary>
    /// A trade transaction.
    /// </summary>
    class Tick
    {
        public string TickerSymbol { get; set; }
        public string Time { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
        public decimal TradePrice { get; set; }
        public int TradeSize { get; set; }
    }
}
