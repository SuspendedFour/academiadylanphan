﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicksAnalyzer
{
    /// <summary>
    /// A summary of trades for a company.
    /// </summary>
    class TickerSummary
    {
        public string TickerSymbol { get; set; }
        public decimal VolumeWeightedAveragePrice { get; set; }
        public decimal LastBid { get; set; }
        public decimal LastAsk { get; set; }
        public decimal LastPrice { get; set; }
        public int LastTradeSize { get; set; }
        public decimal OpenPrice { get; set; }
        public decimal HighPrice { get; set; }
        public decimal LowPrice { get; set; }
        public int Volume { get; set; }
        public int TotalTickCount { get; set; }
    }
}
