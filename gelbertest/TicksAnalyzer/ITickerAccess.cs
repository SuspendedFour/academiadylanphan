﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicksAnalyzer
{
    /// <summary>
    /// An interface for accessing a ticker feed.
    /// </summary>
    interface ITickerAccess
    {
        IEnumerable<Tick> Ticks { get; }
    }
}
