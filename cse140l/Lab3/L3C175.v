module L3C175(SW, KEY, CLOCK_24, LEDR, LEDG, HEX0, HEX1, HEX2, HEX3);
	input [9:0] SW;
	input [3:0] KEY;
	input CLOCK_24;
	output reg [9:0] LEDR;
	output reg [9:0] LEDG;
	output reg [6:0] HEX0;
	output reg [6:0] HEX1;
	output reg [6:0] HEX2;
	output reg [6:0] HEX3;

	localparam startState = 0;
	localparam depositState = 1;
	localparam dispenseState = 2;
	localparam errState = 3;
	localparam errDispenseState = 4;
	localparam creditState = 5;
	localparam dollarState = 6;

	reg [3:0] currentState = startState;
	reg [3:0] nextState = depositState;

	reg [7:0] depositValue = 0;
	reg [7:0] nextDepositValue = 0;

	reg [3:0] coinDispenseCount = 0;
	reg [3:0] nextCoinDispenseCount = 0;

	reg clockReset = 1;
	wire clockOut;
	Clock clock(CLOCK_24, clockReset, 32'd12_000_000, clockOut);

	always @(negedge KEY[3]) begin
		currentState = nextState;
		depositValue = nextDepositValue;
		coinDispenseCount = nextCoinDispenseCount;
	end

	always @(currentState, SW, coinDispenseCount) begin
		LEDR = 0;
		LEDG = 0;
		HEX0 = 7'b1111111;
		HEX1 = 7'b1111111;
		HEX2 = 7'b1111111;
		HEX3 = 7'b1111111;

		clockReset = currentState != dispenseState
			&& currentState != errDispenseState
			&& currentState != creditState
			&& currentState != dollarState;

		if (clockOut) begin
			LEDR = 10'b1111111111;
		end

		if (SW[9] && currentState != errState && currentState != errDispenseState) begin
			HEX0 = binaryToHexDisplay(coinDispenseCount);
			// HEX1 = binaryToHexDisplay(nextCoinDispenseCount);
			// HEX2 = binaryToHexDisplay(currentState);
			LEDG = binaryToHexDisplay(nextState) & 0;
		end
		else begin
			case (currentState)
				startState: begin
					HEX3 = binaryToHexDisplay(0);
					HEX2 = binaryToHexDisplay(1);
					HEX1 = binaryToHexDisplay(7);
					HEX0 = binaryToHexDisplay(5);
				end
				depositState: begin
					HEX3 = binaryToHexDisplay(depositValue / 10);
					HEX2 = binaryToHexDisplay(depositValue % 10);
				end
				dispenseState: begin
					HEX3 = binaryToHexDisplay(3);
					HEX2 = binaryToHexDisplay(5);
					HEX1 = binaryToHexDisplay((depositValue - 35) / 10);
					HEX0 = binaryToHexDisplay((depositValue - 35) % 10);
				end
				errState: begin
					HEX3 = 7'b0000110;
					HEX2 = 7'b0101111;
					HEX1 = 7'b0101111;
				end
				errDispenseState: begin
					HEX3 = 7'b0000110;
					HEX2 = 7'b0101111;
					HEX1 = 7'b0101111;
				end
				creditState: begin
					HEX3 = binaryToHexDisplay(3);
					HEX2 = binaryToHexDisplay(5);
					HEX1 = binaryToHexDisplay(0);
					HEX0 = binaryToHexDisplay(0);
				end
				dollarState: begin
					HEX3 = binaryToHexDisplay(3);
					HEX2 = binaryToHexDisplay(5);
					HEX1 = binaryToHexDisplay((depositValue - 35) / 10);
					HEX0 = binaryToHexDisplay((depositValue - 35) % 10);
				end
			endcase
		end

		case (currentState)
			startState: begin
				nextDepositValue = 0;
				nextState = depositState;
				nextCoinDispenseCount = 0;
			end
			depositState: begin
				nextCoinDispenseCount = coinDispenseCount;

				if (isDepositEmpty(0)) begin
					nextDepositValue = depositValue;
					nextState = currentState;
				end
				else if (!isDepositValid(0)) begin
					nextState = errState;
				end
				else if (SW[3]) begin
					nextDepositValue = depositValue + 100;
					nextState = dollarState;
					nextCoinDispenseCount = coinDispenseCount + 1;
				end
				else if (SW[4]) begin
					nextDepositValue = 35;
					nextState = creditState;
				end
				else if (SW[8]) begin
					nextDepositValue = 0;
					nextState = depositState;
				end
				else begin
					nextDepositValue = depositValue + getDepositInput(0);
					if (nextDepositValue >= 35) begin
						nextState = dispenseState;
						nextCoinDispenseCount = coinDispenseCount + 1;
					end
					else begin
						nextState = depositState;
					end
				end
			end
			dispenseState: begin
				nextCoinDispenseCount = coinDispenseCount;

				if (isDepositEmpty(0)) begin
					nextDepositValue = depositValue;
					nextState = currentState;
				end
				else if (!isDepositValid(0)) begin
					nextState = errDispenseState;
				end
				else if (SW[3]) begin
					nextDepositValue = 100;
					nextState = dollarState;
					nextCoinDispenseCount = coinDispenseCount + 1;
				end
				else if (SW[4]) begin
					if (depositValue == 35) begin
						nextState = errDispenseState;
					end
					else begin
						nextDepositValue = 35;
						nextState = creditState;
					end
				end
				else if (SW[8]) begin
					nextDepositValue = 0;
					nextState = depositState;
				end
				else begin
					nextDepositValue = getDepositInput(0);
					nextState = depositState;
				end
			end
			errState: begin
				nextCoinDispenseCount = coinDispenseCount;
				nextDepositValue = 0;
				nextState = depositState;
			end
			errDispenseState: begin
				nextCoinDispenseCount = coinDispenseCount;
				nextDepositValue = 0;
				nextState = depositState;
			end
			creditState: begin
				nextCoinDispenseCount = coinDispenseCount;

				if (isDepositEmpty(0)) begin
					nextDepositValue = depositValue;
					nextState = currentState;
				end
				else if (!isDepositValid(0)) begin
					nextState = errDispenseState;
				end
				else if (SW[3]) begin
					nextDepositValue = 100;
					nextCoinDispenseCount = coinDispenseCount + 1;
					nextState = dollarState;
				end
				else if (SW[4]) begin
					nextState = errDispenseState;
				end
				else if (SW[8]) begin
					nextDepositValue = 0;
					nextState = depositState;
				end
				else begin
					nextDepositValue = getDepositInput(0);
					nextState = depositState;
				end
			end
			dollarState: begin
				nextCoinDispenseCount = coinDispenseCount;

				if (isDepositEmpty(0)) begin
					nextDepositValue = depositValue;
					nextState = currentState;
				end
				else if (!isDepositValid(0)) begin
					nextState = errDispenseState;
				end
				else if (SW[3]) begin
					nextState = errDispenseState;
				end
				else if (SW[4]) begin
					nextDepositValue = 35;
					nextState = creditState;
				end
				else if (SW[8]) begin
					nextDepositValue = 0;
					nextState = depositState;
				end
				else begin
					nextDepositValue = getDepositInput(0);
					nextState = depositState;
				end
			end
		endcase
	end

	function isDepositEmpty;
		input dummy;
		isDepositEmpty = ~(|SW[4:0] | SW[8]);
	endfunction

	function isDepositValid;
		input dummy;
		isDepositValid = {SW[4:0], SW[8]} == 6'b000001;
		isDepositValid = isDepositValid || {SW[4:0], SW[8]} == 6'b000010;
		isDepositValid = isDepositValid || {SW[4:0], SW[8]} == 6'b000100;
		isDepositValid = isDepositValid || {SW[4:0], SW[8]} == 6'b001000;
		isDepositValid = isDepositValid || {SW[4:0], SW[8]} == 6'b010000;
		isDepositValid = isDepositValid || {SW[4:0], SW[8]} == 6'b100000;
	endfunction

	function [7:0] getDepositInput;
		input dummy;
		if (SW[0]) begin
			getDepositInput = 5;
		end
		else if (SW[1]) begin
			getDepositInput = 10;
		end
		else if (SW[2]) begin
			getDepositInput = 25;
		end
	endfunction

	function [6:0] binaryToHexDisplay;
		input [3:0] binaryIn;
		case (binaryIn)
			4'h0: binaryToHexDisplay = 7'b1000000;
			4'h1: binaryToHexDisplay = 7'b1111001;
			4'h2: binaryToHexDisplay = 7'b0100100;
			4'h3: binaryToHexDisplay = 7'b0110000;
			4'h4: binaryToHexDisplay = 7'b0011001;
			4'h5: binaryToHexDisplay = 7'b0010010;
			4'h6: binaryToHexDisplay = 7'b0000010;
			4'h7: binaryToHexDisplay = 7'b1111000;
			4'h8: binaryToHexDisplay = 7'b0000000;
			4'h9: binaryToHexDisplay = 7'b0011000;
			4'hA: binaryToHexDisplay = 7'b0001000;
			4'hB: binaryToHexDisplay = 7'b0000011;
			4'hC: binaryToHexDisplay = 7'b1000110;
			4'hD: binaryToHexDisplay = 7'b0100001;
			4'hE: binaryToHexDisplay = 7'b0000110;
			4'hF: binaryToHexDisplay = 7'b0001110;
			default: binaryToHexDisplay = 7'b0110110;
		endcase
	endfunction
endmodule

// ticks - ticks per period
module Clock(clk, rst, ticks, out);
	input clk;
	input rst;
	input [31:0] ticks;
	output reg out;

	reg [31:0] counter;

	always @(posedge clk) begin
		if (rst) begin
			counter = 0;
		end
		else begin
			counter = (counter + 1) % ticks;
		end

		out = counter > ticks / 2;
	end
endmodule