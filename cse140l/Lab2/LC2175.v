module L2C175(SW, KEY, CLOCK_24, LEDR, LEDG, HEX0, HEX1, HEX2, HEX3);
	input [9:0] SW;
	input [3:0] KEY;
	input CLOCK_24;
	output reg [9:0] LEDR;
	output reg [9:0] LEDG;
	output reg [6:0] HEX0;
	output reg [6:0] HEX1;
	output reg [6:0] HEX2;
	output reg [6:0] HEX3;

	reg [31:0] i;

	wire part3Enabled;
	assign part3Enabled = SW[7] && !SW[5] && !SW[6];

	wire part4Enabled;
	assign part4Enabled = SW[8] && !SW[5] && !SW[6] && !SW[7];

	wire part5Enabled;
	assign part5Enabled = SW[9] && !SW[5] && !SW[6] && !SW[7] & !SW[8];

	wire [3:0] part3CounterOut;
	Counter4Bit part3Counter(!KEY[2], !SW[1], SW[0] | !part3Enabled, part3CounterOut);

	wire part4ClockOut;
	wire [31:0] part4CounterOut;
	Clock part4Clock(CLOCK_24, !part4Enabled | SW[0], 32'd24_000_000, part4ClockOut);
	Counter part4Counter(part4ClockOut, !part4Enabled | SW[0], 3 * 3 * 3 * 3, part4CounterOut);

	wire ballClockOut;
	wire [4:0] ballState;
	wire messageClockOut;
	wire [4:0] messageState;
	Clock ballClock(CLOCK_24, !part5Enabled | SW[0], 31'd24_000_000 / 19, ballClockOut);
	Clock messageClock(CLOCK_24, !part5Enabled | SW[0], 31'd24_000_000, messageClockOut);
	Counter ballStateCounter(ballClockOut, !part5Enabled, 19, ballState);
	Counter messageStateCounter(messageClockOut, !part5Enabled, 19, messageState);
	wire [132:0] message;
	assign message = 133'b1111111_1111111_1111111_1111111_1001000_0110000_1110001_1110001_0000001_1111111_1111111_0110001_1111001_1000010_1111111_1001111_0001111_0100100_1111111;

	always @(SW, KEY, part3Counter) begin
		LEDR = 0;
		LEDG = 0;
		HEX0 = 7'b1111111;
		HEX1 = 7'b1111111;
		HEX2 = 7'b1111111;
		HEX3 = 7'b1111111;

		if (SW[5]) begin
			if (SW[3:0] < 10) begin
				HEX3 = binaryToHexDisplay(4'b0);
				HEX2 = binaryToHexDisplay(SW[3:0]);
			end
			else begin
				HEX3 = binaryToHexDisplay(4'b1);
				HEX2 = binaryToHexDisplay(SW[3:0] - 10);
			end

			HEX0 = binaryToHexDisplay(SW[3:0]);
		end
		else if (SW[6]) begin
			HEX3 = binaryToHexDisplay(SW[4:3]);
			HEX2 = binaryToHexDisplay(SW[2:1]);

			if (SW[0])
				HEX0 = binaryToHexDisplay(SW[4:3] * SW[2:1]);
			else
				HEX0 = binaryToHexDisplay(SW[4:3] + SW[2:1]);
		end
		else if (SW[7]) begin
			HEX2 = binaryToHexDisplay(part3CounterOut);
		end
		else if (SW[8]) begin
			LEDG[0] = part4ClockOut;

			HEX0 = binaryToHexDisplay(part4CounterOut % 3);
			HEX1 = binaryToHexDisplay(part4CounterOut / 3 % 3);
			HEX2 = binaryToHexDisplay(part4CounterOut / 3 / 3 % 3);
			HEX3 = binaryToHexDisplay(part4CounterOut / 3 / 3 / 3 % 3);
		end
		else if (SW[9]) begin
			if (ballState < 10) begin
				LEDR[ballState] = 1;
			end
			else begin
				LEDR[19 - ballState] = 1;
			end

			for (i = 0; i < 7; i = i + 1) begin
				HEX3[i] = message[132 - messageState * 7 - i];
				HEX2[i] = message[132 - (messageState + 1) % 19 * 7 - i];
				HEX1[i] = message[132 - (messageState + 2) % 19 * 7 - i];
				HEX0[i] = message[132 - (messageState + 3) % 19 * 7 - i];
			end
		end
		else begin
			HEX2 = binaryToHexDisplay(1);
			HEX1 = binaryToHexDisplay(7);
			HEX0 = binaryToHexDisplay(5);
		end
	end
	
	function [6:0] binaryToHexDisplay;
		input [3:0] binaryIn;
		case (binaryIn)
			4'h0: binaryToHexDisplay = 7'b1000000;
			4'h1: binaryToHexDisplay = 7'b1111001;
			4'h2: binaryToHexDisplay = 7'b0100100;
			4'h3: binaryToHexDisplay = 7'b0110000;
			4'h4: binaryToHexDisplay = 7'b0011001;
			4'h5: binaryToHexDisplay = 7'b0010010;
			4'h6: binaryToHexDisplay = 7'b0000010;
			4'h7: binaryToHexDisplay = 7'b1111000;
			4'h8: binaryToHexDisplay = 7'b0000000;
			4'h9: binaryToHexDisplay = 7'b0011000;
			4'hA: binaryToHexDisplay = 7'b0001000;
			4'hB: binaryToHexDisplay = 7'b0000011;
			4'hC: binaryToHexDisplay = 7'b1000110;
			4'hD: binaryToHexDisplay = 7'b0100001;
			4'hE: binaryToHexDisplay = 7'b0000110;
			4'hF: binaryToHexDisplay = 7'b0001110;
			default: binaryToHexDisplay = 7'b0110110;
		endcase
	endfunction
endmodule

module Counter4Bit(trigger, countUp, reset, out);
	input trigger;
	input reset;
	input countUp;
	output reg [3:0] out;

	always @(posedge trigger or posedge reset) begin
		if (reset) begin
			out = 0;
		end
		else begin
			out = countUp ? out + 1 : out - 1;
		end
	end
endmodule

module Counter(trigger, rst, max, out);
	input trigger;
	input rst;
	input [31: 0] max;
	output reg [31:0] out;

	always @(posedge trigger or posedge rst) begin
		if (rst) begin
			out = 0;
		end
		else begin
			out = (out + 1) % max;
		end
	end
endmodule

// ticks - ticks per period
module Clock(clk, rst, ticks, out);
	input clk;
	input rst;
	input [31:0] ticks;
	output reg out;

	reg [31:0] counter;

	always @(posedge clk) begin
		if (rst) begin
			counter = 0;
		end
		else begin
			counter = (counter + 1) % ticks;
		end

		out = counter > ticks / 2;
	end
endmodule